# Salon Appointment Scheduler

### Purpose:
This application aims to let the user arrange salon appointments in various countries and conduct business in multiple languages. 
In addition to that, the application has a login that checks for login attempts that will be written in the log.txt.  
Also, the application lets the user create, update, and delete the client record and appointment. 
Lastly, it reports the total number of client appointments by type and week, the schedule for each stylist, and number of appointment by location.

### Project Information:
```
Author: Christopher Lim
Contact: clim18@my.wgu.edu
Version: 1.0
Date: 11/26/2023

IDE: IntelliJ 2023.2.5 (Community Edition)
JDK: 17.0.9 2023-10-17 LTS
JavaFX: 17.0.6
MySQL Connector: 8.2.0
```

### Instructions:
- Open the project in IntelliJ IDEA.
- Make sure JDK 17.0.9 and JavaFX 17.0. 6 are properly configured.
- Make sure mysql-connector-java-8.2.0 is included in the library.
- Run the Main class to start the application.
- Log in with username "altUser", password "altPSW".


### Additional Report:
The extra report will display the total number of appointments by location. 
I will list every location with an appointment and show the overall number of appointments there.

### Lambda Expression:
To do List:
- [X] checkUserNamePassword
- [X] setUpdateClientViewClientCountry
- [X] alert.showAndWait().ifPresent((confirmButton -> {

### Personal Gitlabs repository:
https://gitlab.com/wgu-java-projects/wgu-c195-alt

### Images:
![altdb](src/main/resources/images/altdb.png)
![altdb-uml](src/main/resources/images/altdb-uml.png)
![en-login](src/main/resources/images/en-login.png)
![es-login](src/main/resources/images/es-login.png)
![client-table](src/main/resources/images/client-table.png)
![add-client](src/main/resources/images/add-client.png)
![update-client](src/main/resources/images/update-client.png)
![unique-id](src/main/resources/images/unique-id.png)
![get-all-country](src/main/resources/images/get-all-country.png)
![add-client-sq](src/main/resources/images/add-client-sql.png)
![get-all-client](src/main/resources/images/get-all-client.png)
![add-client-alert](src/main/resources/images/add-client-alert.png)
![delete-client-db](src/main/resources/images/delete-client.png)
![delete-client-alert](src/main/resources/images/delete-client-alert.png)
![update-client-db](src/main/resources/images/client-update-sql.png)
![appointment-table-view](src/main/resources/images/appointment-table-view.png)
![report-table-view](src/main/resources/images/report-table-view.png)
![appt-sql](src/main/resources/images/appointment-sql.png)
![add-apt](src/main/resources/images/add-apt.png)
![edit-apt](src/main/resources/images/edit-apt.png)
![get-all-stylist-sql](src/main/resources/images/sql-get-all-stylist.png)
![get-max-appt-id](src/main/resources/images/sql-get-max-apptid.png)
![appt-alert-test](src/main/resources/images/test-add-appt.png)
![add-appt-sql](src/main/resources/images/add-appt.png)
![delete-alert](src/main/resources/images/delete-alert.png)
![delete-appt-sql](src/main/resources/images/delete-appt-sql.png)
![edit-appt-sql](src/main/resources/images/edit-appt-sql.png)
![edit-appt-alert](src/main/resources/images/edit-appt-alert.png)
![sort-by-appt](src/main/resources/images/sort-by-appt.png)
![sort-by-month-appt](src/main/resources/images/sort-by-month-appt.png)
![sort-by-month-week](src/main/resources/images/sort-by-week-appt.png)
![appt-sort-sql](src/main/resources/images/appt-sort-sql.png)
![appt-sort-demo](src/main/resources/images/appt-sort-demo.png)
![appt-type-query](src/main/resources/images/appt-type-query.png)
![appt-type-week-query](src/main/resources/images/appt-type-week-query.png)
![report-style-alert](src/main/resources/images/report-style-alert.png)
![report-style-found](src/main/resources/images/report-style-found.png)
![appt-location-sql](src/main/resources/images/appt-location-sql.png)
![report-location-sql](src/main/resources/images/report-location-sql.png)
![report-stylist-sql](src/main/resources/images/report-stylist-sql.png)
![datetime-sql](src/main/resources/images/datetime-sql.png)
![appt-alert](src/main/resources/images/appt-alert.png)
![no-appt-alert](src/main/resources/images/no-appt-alert.png)
