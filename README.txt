Salon Appointment Scheduler

Purpose:
This application aims to let the user arrange salon appointments in various countries and conduct business in multiple languages.
In addition to that, the application has a login that checks for login attempts that will be written in the log.txt.
Also, the application lets the user create, update, and delete the client record and appointment.
Lastly, it reports the total number of client appointments by type and week, the schedule for each stylist, and number of appointment by location.

Author: Christopher Lim
Contact: clim18@my.wgu.edu
Version: 1.0
Date: 11/26/2023

IDE: IntelliJ 2023.2.5 (Community Edition)
JDK: 17.0.9 2023-10-17 LTS
JavaFX: 17.0.6
MySQL Connector: 8.2.0

Instructions:
- Open the project in IntelliJ IDEA.
- Make sure JDK 17.0.9 and JavaFX 17.0. 6 are properly configured.
- Make sure mysql-connector-java-8.2.0 is included in the library.
- Run the Main class to start the application.
- Log in with username "altUser", password "altPSW".

Additional Report:
The extra report will display the total number of appointments by location.
I will list every location with an appointment and show the overall number of appointments there.

Personal Gitlabs repository:
https://gitlab.com/wgu-java-projects/wgu-c195-alt