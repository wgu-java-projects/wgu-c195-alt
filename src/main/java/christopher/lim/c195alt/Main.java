package christopher.lim.c195alt;

import christopher.lim.c195alt.helper.JavaDataBaseConnectivity;
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

import java.util.Locale;
import java.util.Objects;
import java.util.ResourceBundle;


/**
 * This Main class will connect the app in the database
 * and help load the login screen
 * @author Christopher Lim
 */
public class Main extends Application {
    @Override
    public void start(Stage stage) throws Exception{
        Locale currentLocale = Locale.getDefault();
        ResourceBundle bundle = ResourceBundle.getBundle("/lang", currentLocale);
        Parent root = FXMLLoader.load(Objects.requireNonNull(getClass().getResource("/christopher/lim/c195alt/view/LoginView.fxml")),bundle);
        stage.setTitle("Salon Appointment Scheduler");
        stage.setScene(new Scene(root));
        stage.show();
    }

    public static void main(String[] args) {
        JavaDataBaseConnectivity.openConnectionDatabase();
        launch(args);
        JavaDataBaseConnectivity.closeConnectionDatabase();
    }
}
