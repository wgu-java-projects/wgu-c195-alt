package christopher.lim.c195alt.helper;

import java.sql.Connection;
import java.sql.DriverManager;

/**
 * An abstract class that contains Java DataBase Connectivity
 * @author Christopher Lim
 */
public abstract class JavaDataBaseConnectivity {
    /**
     * Fields needed to connect to the local Database
     */
    private static final String javaDataBaseConnectivity = "jdbc:mysql://localhost/altdb?connectionTimeZone = SERVER";
    private static final String driver = "com.mysql.cj.jdbc.Driver";
    private static final String userName = "sqlUser";
    private static final String password = "Passw0rd!";
    public static Connection connection;

    /**
     * This method will open the connection to the altdb. If successful,
     * it will print out "Connection to the Database is Open!" in the console.log.
     * And the User should be able to login. If not, an error message will be printed.
     */
    public static void openConnectionDatabase(){
        System.out.println(javaDataBaseConnectivity);
        try {
            Class.forName(driver);
            connection = DriverManager.getConnection(javaDataBaseConnectivity, userName, password);
            System.out.println("Connection to the Database is Open!");
        }catch(Exception e){
            System.out.println("Connection to the Database has Failed!");
            System.out.println(e);
        }
    }

    /**
     * This method will close the connection to the altdb. It will
     * be triggered if the User closes the App by clicking the exit button.
     * Or the user close the app by clicking the window's X button.
     */
    public static void closeConnectionDatabase(){
        System.out.println(javaDataBaseConnectivity);
        try {
            connection.close();
            System.out.println("Closing the Connection to the Database is now Closed!");
        } catch(Exception e) {
            System.out.println("Closing the Connection to the Database has Failed!");
            System.out.println(e);
        }
    }

}
