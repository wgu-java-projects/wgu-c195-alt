package christopher.lim.c195alt.helper;

import javafx.event.ActionEvent;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.stage.Stage;

import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.time.LocalDateTime;
import java.time.ZoneId;

/**
 * An abstract class that contains Utilities that will be used for
 * the whole Salon Appointment Application
 * @author Christopher Lim
 */
public abstract class Utilities {
    /**
     * This helper method is being used to navigate to the next screen
     * @param actionEvent the action event is triggered when clicking a button that will navigate to the next screen
     * @param fxmlFile the fxmlFile is a String location path to the view
     * @throws IOException will be triggered if I/O exception to some sort has occurred.
     */
    public static void navigateApplicationView(ActionEvent actionEvent, String fxmlFile) throws IOException {
        // We are passing the fxmlFile path as a string to be used where to navigate next
        FXMLLoader fxmlLoader = new FXMLLoader(Utilities.class.getResource(fxmlFile));
        Scene scene = new Scene(fxmlLoader.load());
        // The action event will trigger the scene change
        Stage stage = (Stage) ((Node)actionEvent.getSource()).getScene().getWindow();
        stage.setScene(scene);
        stage.show();
    }

    /**
     * This method is  to track user activity by capturing log-in attempts,
     * dates, passwords, time stamps, and success rates in log.txt file.
     * Add each new record to the file and save to the program root folder.
     * @param username the username of the user will be captured
     * @param password the password of the user will be captured
     * @param message the message that will be passed later
     * @throws IOException will be triggered if I/O exception to some sort has occurred.
     */
    public static void loginActivityLog(String username, String password, String message) throws IOException {
        // Gets the users current date and time
        LocalDateTime localDateTime = LocalDateTime.now();
        // This is the file name that is on the Performance Assessment
        String fileName = "log.txt";

        // This sets where the log will create and if the current text will be appended
        FileWriter fileWriter = new FileWriter(fileName, true);
        // This will print the logs
        PrintWriter printWriter = new PrintWriter(fileWriter);

        // This will check if username and password is blank or empty.
        // And if it's either the value will return null
        if (username.isBlank() || username.isEmpty()){
            username = null;
        } else if (password.isBlank() || password.isEmpty()) {
            password = null;
        }
        // This will write the exact log to the log.txt
        printWriter.println("Username: " + username + ", Password: " + password + ", " + message + ", " + localDateTime + " (" + ZoneId.systemDefault() + ")");

        // This close the log.txt
        printWriter.close();
    }
}
