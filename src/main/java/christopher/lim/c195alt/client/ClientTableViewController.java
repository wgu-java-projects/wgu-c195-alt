package christopher.lim.c195alt.client;

import christopher.lim.c195alt.helper.Utilities;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.stage.Stage;

import java.io.IOException;
import java.net.URL;
import java.sql.SQLException;
import java.util.Optional;
import java.util.ResourceBundle;

/**
 * This controller class is for the Client Table View that will be used
 * to display all the clients in a table from the altdb. And it will let the user
 * Add, Edit and Delete Clients.
 * @author Christopher Lim
 */
public class ClientTableViewController implements Initializable {
    // These are the list of fx:id that will be used for the client table view
    @FXML private TableView<ClientModel> clientModelTableViewTableView;
    @FXML private TableColumn<ClientModel, Integer> clientViewClientIdColumn;
    @FXML private TableColumn<ClientModel, String> clientViewClientNameColumn;
    @FXML private TableColumn<ClientModel, String> clientViewClientEmailColumn;
    @FXML private TableColumn<ClientModel, String> clientViewClientHairColorColumn;
    @FXML private TableColumn<ClientModel, String> clientViewClientPostalCodeColumn;
    @FXML private TableColumn<ClientModel, String> clientViewClientStateProvinceColumn;
    @FXML private TableColumn<ClientModel, String> clientViewClientCountryColumn;
    @FXML private TableColumn<ClientModel, Integer> clientViewClientActiveColumn;

    /**
     * This method will navigate the User to the Appointment Table View.
     * This table will have appointment id, title, description, location, type, start date and time,
     * end date and time, client id and style id.
     * @param actionEvent the action will be triggered when the Appointment Table View button is clicked.
     * @throws IOException will be triggered if I/O exception to some sort has occurred.
     */
    @FXML
    public void onActionAppointmentTableViewButton(ActionEvent actionEvent) throws IOException {
        Utilities.navigateApplicationView(actionEvent, "/christopher/lim/c195alt/view/AppointmentTableView.fxml");
    }

    /**
     * This method will navigate the User to the Report Table View.
     * It will have schedules for Stylist and clients appointments by type and week
     * @param actionEvent the action will be triggered when the Report Table View is clicked
     * @throws IOException will be triggered if I/O exception to some sort has occurred.
     */
    @FXML
    public void onActionReportTableViewButton(ActionEvent actionEvent) throws IOException {
        Utilities.navigateApplicationView(actionEvent, "/christopher/lim/c195alt/view/ReportTableView.fxml");
    }

    /**
     * This method will navigate to the Add Client View. And the user needs Clients Name, Email,
     * Preferred Hair Color, Postal Code, Active Status, State/Province and Country.
     * The Client ID will be auto generated and will be disabled.
     * @param actionEvent the action event will be triggered once the Adding Client Button is clicked.
     * @throws IOException will be triggered if I/O exception to some sort has occurred.
     */
    @FXML
    public void onActionAddingClientButton(ActionEvent actionEvent) throws IOException {
        Utilities.navigateApplicationView(actionEvent, "/christopher/lim/c195alt/view/AddingClientView.fxml");
    }

    /**
     * This method will navigate to the selected Client from the table view. If nothing
     * is selected the screen will not navigate to the Update Client View. Instead, it
     * will show an alert message telling the User, they need to click of the client
     * from the table.
     * @param actionEvent the action event will be triggered when the Editing Client button is click
     */
    @FXML
    public void onActionEditingClientButton(ActionEvent actionEvent) {
        if (clientModelTableViewTableView.getSelectionModel().isEmpty()) {
            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.setTitle("Error");
            alert.setHeaderText("No Client is Selected!");
            alert.setContentText("Please make sure, you have selected a Client from the row.");
            alert.showAndWait();
        } else {
            ClientModel clientModelEditing = clientModelTableViewTableView.getSelectionModel().getSelectedItem();

            try {
               Stage stage = (Stage) ((Node) actionEvent.getSource()).getScene().getWindow();

               if(clientModelEditing == null) {
                   System.out.println("Selected Client is missing.");
                   return;
               }

                FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("/christopher/lim/c195alt/view/EditingClientView.fxml"));
                stage.setScene(new Scene(fxmlLoader.load()));

                EditingClientViewController editingClientViewController = fxmlLoader.getController();
                editingClientViewController.editingInitialForm(clientModelEditing);
                stage.show();
            } catch (IOException e) {
                System.out.println(e);
            }
        }
    }

    /**
     * This method will remove a Client from the altdb. If you selected a Client
     * from the table view and click Removing Client, there will be an alert that will
     * ask you if you want to delete the Client. If you click the button without selecting
     * a Client, an alert will be prompt telling you that the deletion failed.
     * @param actionEvent the action event will be triggered when clicking the Removing Client Button
     * @throws IOException will be triggered if I/O exception to some sort has occurred.
     */
    @FXML
    public void onActionRemovingClientButton(ActionEvent actionEvent) throws IOException {
        if (clientModelTableViewTableView.getSelectionModel().isEmpty()) {
            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.setTitle("Error");
            alert.setHeaderText("No Client is Selected!");
            alert.setContentText("Please make sure, you have selected a Client from the row");
            alert.showAndWait();
        } else {
            ClientModel clientModel = clientModelTableViewTableView.getSelectionModel().getSelectedItem();

            int clientId = clientModel.getIdclient();
            String clientName = clientModel.getName();

            Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
            alert.setTitle("Confirmation");
            alert.setHeaderText("Delete Client?");
            alert.setContentText("Are you sure, you want to delete this Client?\n" +
                    "Client Id: " + clientId +
                    "\nClient Name: " + clientName);

            Optional<ButtonType> buttonAction = alert.showAndWait();

            if (buttonAction.isPresent() && buttonAction.get() == ButtonType.OK) {
                try {
                    if (ClientSQLQuery.deletingClientFromAltDB(clientId)) {
                        System.out.println("Client: " + clientName + " is now deleted!");
                    } else {
                        System.out.println("Client: " + clientName + " is NOT deleted!");
                    }
                    clientModelTableViewTableView.setItems(ClientSQLQuery.getAllClientQuery());
                } catch (SQLException e) {
                    throw new RuntimeException(e);
                }
            }
        }
    }

    /**
     * This method will log out the User and navigate them back to the Login View
     * @param actionEvent the action event is being triggered when the Log out button is clicked.
     * @throws IOException will be triggered if I/O exception to some sort has occurred.
     */
    @FXML
    public void onActionLogoutButton(ActionEvent actionEvent) throws IOException {
        Utilities.navigateApplicationView(actionEvent, "/christopher/lim/c195alt/view/LoginView.fxml");
    }

    /**
     * This method will close the whole application.
     * @param actionEvent the action event is being triggered when the exit button is clicked.
     * @throws IOException will be triggered if I/O exception to some sort has occurred.
     */
    @FXML
    public void onActionExitButton(ActionEvent actionEvent) throws IOException {
        System.exit(0);
    }

    /**
     * This initializes the Client Table View page
     * @param url this sets the url
     * @param resourceBundle this sets the resource bundle
     */
    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        try {
            ObservableList<ClientModel> clientModels = ClientSQLQuery.getAllClientQuery();
            clientModelTableViewTableView.setItems(clientModels);
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }

        // This sets the cell value of the table for Clients
        clientViewClientIdColumn.setCellValueFactory(new PropertyValueFactory<ClientModel, Integer>("idclient"));
        clientViewClientNameColumn.setCellValueFactory(new PropertyValueFactory<ClientModel, String>("name"));
        clientViewClientEmailColumn.setCellValueFactory(new PropertyValueFactory<ClientModel, String>("email"));
        clientViewClientHairColorColumn.setCellValueFactory(new PropertyValueFactory<ClientModel, String>("haircolor"));
        clientViewClientPostalCodeColumn.setCellValueFactory(new PropertyValueFactory<ClientModel, String>("pcode"));
        clientViewClientStateProvinceColumn.setCellValueFactory(new PropertyValueFactory<ClientModel, String>("st_pv"));
        clientViewClientCountryColumn.setCellValueFactory(new PropertyValueFactory<ClientModel, String>("country"));
        clientViewClientActiveColumn.setCellValueFactory(new PropertyValueFactory<ClientModel, Integer>("active"));
    }
}
