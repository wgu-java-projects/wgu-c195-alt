package christopher.lim.c195alt.client;

import christopher.lim.c195alt.helper.Utilities;
import christopher.lim.c195alt.appointment.CountryModel;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.*;

import java.io.IOException;
import java.net.URL;
import java.sql.SQLException;
import java.util.Objects;
import java.util.ResourceBundle;

/**
 * This controller class is for the Add Client View that will be used
 * to add new Client to the altdb. And it will add the new record
 * to the Client Table View.
 * @author Christopher Lim
 */
public class EditingClientViewController implements Initializable {
    // These are the list of fx:id that will be used for the editing client view
    @FXML private TextField updateClientViewClientId;
    @FXML private TextField updateClientViewClientName;
    @FXML private TextField updateClientViewClientEmail;
    @FXML private TextField updateClientViewHairColor;
    @FXML private TextField updateClientViewPostalCode;
    @FXML private  TextField updateClientViewStateProvince;
    @FXML private ComboBox<CountryModel> updateClientViewClientCountry;
    @FXML private CheckBox updateClientViewActive;


    // This field will be used to check if the Client is active or not
    int activeStatus = 0;;

    /**
     * This method is being used by the Active Checkbox, in which if the checkbox is selected
     * it will return the value of 1. And if its unchecked, it will return the value of 0.
     * @param actionEvent the action event will be triggered once the active checkbox is selected
     * @throws IOException will be triggered if I/O exception to some sort has occurred.
     */
    @FXML
    public void setActiveCheckbox(ActionEvent actionEvent) throws IOException {
        if (updateClientViewActive.isSelected()) {
            Alert alert = new Alert(Alert.AlertType.INFORMATION);
            alert.setTitle("Information");
            alert.setHeaderText("Client's Active Status");
            alert.setContentText("You just make the client Active.");
            alert.showAndWait();

            activeStatus = 1;
        } else {
            activeStatus = 0;
        }
    }

    /**
     * This method will set the Update Client View for Country drop down.
     * LAMBDA EXPRESSION: The "countryModel -> Objects.equals(countryModel.getCountryName(), clientModelEditing.getCountry()"
     * is used for filtering the Country name of the selected Client and set it on Country Dropdown.
     * @param clientModelEditing the client model that will be updating
     * @return It will return the Country name of the selected Client
     */
    public CountryModel setUpdateClientViewClientCountry(ClientModel clientModelEditing) {
        return updateClientViewClientCountry.getItems().stream()
                .filter(countryModel -> Objects.equals(countryModel.getCountryName(), clientModelEditing.getCountry()))
                .findFirst()
                .orElse(null);
    }

    /**
     * This method will initiate the fields, dropdown and checkbox with the Client's selected
     * data object.
     * @param clientModelEditing the clientModelEditing will access the Client Model to fill
     * up the fields, dropdown and checkbox.
     * @throws SQLException this will give out information about database access error
     */
    public void editingInitialForm(ClientModel clientModelEditing) {
        // This fill up the Client Update Form
        updateClientViewClientId.setText(Integer.toString((clientModelEditing.getIdclient())));
        updateClientViewClientName.setText(clientModelEditing.getName());
        updateClientViewClientEmail.setText(clientModelEditing.getEmail());
        updateClientViewHairColor.setText(clientModelEditing.getHaircolor());
        updateClientViewPostalCode.setText(clientModelEditing.getPcode());
        updateClientViewStateProvince.setText(clientModelEditing.getSt_pv());

        // This will grab the Clients Country
        updateClientViewClientCountry.getSelectionModel().select(setUpdateClientViewClientCountry(clientModelEditing));

        // This checks for the Active Status
        if (clientModelEditing.getActive() == 1) {
            System.out.println("Current User Active Status: " + clientModelEditing.getActive());
            updateClientViewActive.setSelected(true);
        } else {
            System.out.println("Current User Active Status: " + clientModelEditing.getActive());
            updateClientViewActive.setSelected(false);
        }
    }


    /**
     * This method will update Clients Model in the altdb with any changes on the form.
     * And if its successful, it will navigate the User back to the Client Table View
     * @param actionEvent the action event will be triggered when the Updating Client Button is clicked
     * @throws IOException will be triggered if I/O exception to some sort has occurred.
     */
    public void onUpdatingClientButton(ActionEvent actionEvent) throws IOException {
        // The fields for grabbing the data from the text field
        int updatingClientViewClientId = Integer.parseInt(updateClientViewClientId.getText());
        String updatingClientViewClientNameText = updateClientViewClientName.getText();
        String updatingClientViewClientEmailText = updateClientViewClientEmail.getText();
        String updatingClientViewHairColorText = updateClientViewHairColor.getText();
        String updatingClientViewPostalCodeText = updateClientViewPostalCode.getText();
        String updatingClientViewStateStateProvinceText = updateClientViewStateProvince.getText();

        // This field will get the value of the country selected from the dropdown
        String countryName = updateClientViewClientCountry.getSelectionModel().getSelectedItem().getCountryName();

        // This checks if the Active Checkbox is selected
        // if they are check returned 1 and if not return 0
        if (updateClientViewActive.isSelected()) {
            activeStatus = 1;
        } else {
            activeStatus = 0;
        }

        // This if statement will check if the value of the fields are empty.
        // If they are empty, an alert message will appear that the fields must be filled.
        if (updatingClientViewClientNameText == "" || updatingClientViewClientEmailText == "" ||
                updatingClientViewHairColorText == "" || updatingClientViewPostalCodeText =="" || updatingClientViewStateStateProvinceText == "") {
            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.setTitle("Error");
            alert.setHeaderText("Every field needs to be filled out!");
            alert.setContentText("Please check to see if any fields need to be filled up.");
            alert.showAndWait();
        } else {
            try {
                // This will create a new Client Model Object and inject the current or update data
                ClientModel updateClient = new ClientModel(updatingClientViewClientId, updatingClientViewClientNameText,
                        updatingClientViewClientEmailText, updatingClientViewHairColorText, updatingClientViewPostalCodeText,
                        updatingClientViewStateStateProvinceText, countryName, activeStatus);
                // This will run the SQL to update the Client Model
                ClientSQLQuery.updatingClientFromAltDB(updateClient);
                //This will navigate the User Back to the Client Table View
                Utilities.navigateApplicationView(actionEvent, "/christopher/lim/c195alt/view/ClientTableView.fxml");
            } catch (SQLException e) {
                throw new RuntimeException(e);
            }
        }
    }

    /**
     * This method will cancel updating the client form and will navigate back to the Client Table View
     * @param actionEvent the event will be triggered when the Cancel Update Client is clicked
     * @throws IOException will be triggered if I/O exception to some sort has occurred.
     */
    @FXML
    public void onActionCancelUpdatingClientButton(ActionEvent actionEvent) throws IOException {
        Utilities.navigateApplicationView(actionEvent, "/christopher/lim/c195alt/view/ClientTableView.fxml");
    }


    /**
     * This initializes the Editing Client Table View page
     * @param url this sets the url
     * @param resourceBundle this sets the resource bundle
     */
    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        try {
            updateClientViewClientCountry.setItems(ClientSQLQuery.getAllClientCountryQuery());
            updateClientViewClientCountry.getSelectionModel().selectFirst();
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }

    }
}
