package christopher.lim.c195alt.client;

/**
 * A class to create the Client object in the altdb
 * @author Christopher Lim
 */
public class ClientModel {
    private int idclient;
    private String name;
    private String email;
    private String haircolor;
    private String pcode;
    private String st_pv;
    private String country;
    private int active;

    /**
     * This is the class constructor for the Client Model
     * @param idclient the id of the client
     * @param name the name of the client
     * @param email the email of the client
     * @param haircolor the hair color of the client
     * @param pcode the postal code of the client
     * @param st_pv the state and price of the client
     * @param country the country of the client
     * @param active the active status of the client
     */
    public ClientModel(int idclient, String name, String email, String haircolor, String pcode, String st_pv, String country, int active) {
        this.idclient = idclient;
        this.name = name;
        this.email = email;
        this.haircolor = haircolor;
        this.pcode = pcode;
        this.st_pv = st_pv;
        this.country = country;
        this.active = active;
    }

    /**
     * This method gets the client id from the altdb
     * @return the client id
     */
    public int getIdclient() {
        return idclient;
    }

    /**
     * This method will set the client id in the new client form
     * @param idclient the client id will be set to a new client
     */
    public void setIdclient(int idclient) {
        this.idclient = idclient;
    }

    /**
     * This method will get the name of the client  from the altdb
     * @return the name will be return after the method is called
     */
    public String getName() {
        return name;
    }

    /**
     * This method sets the name of the client on the clients form
     * @param name the name is returned and saved to the db
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * This method gets the email of the client from the altdb
     * @return the email is being returned as string
     */
    public String getEmail() {
        return email;
    }

    /**
     * This method sets email in the Client form
     * @param email the email param is added to the altdb
     */
    public void setEmail(String email) {
        this.email = email;
    }

    /**
     * This method getting the Clients hair color from the altdb
     * @return the hair color of the client is being return
     */
    public String getHaircolor() {
        return haircolor;
    }

    /**
     * This method is sets the hair color of the client in the new form
     * @param haircolor the hair color is being set to the altdb
     */
    public void setHaircolor(String haircolor) {
        this.haircolor = haircolor;
    }

    /**
     * This method gets the postal code from the altdb
     * @return the postal code of the client
     */
    public String getPcode() {
        return pcode;
    }

    /**
     * This method sets the postal code of the Client
     * @param pcode the pcode or postal code is being sets in the altdb
     */
    public void setPcode(String pcode) {
        this.pcode = pcode;
    }

    /**
     * This method gets the state or province of the Client from the altdb
     * @return the st_pv returns the state or province
     */
    public String getSt_pv() {
        return st_pv;
    }

    /**
     * This method sets the state or province of the Client from the form
     * @param st_pv the st_pv or state/province is being set up in the altdb
     */
    public void setSt_pv(String st_pv) {
        this.st_pv = st_pv;
    }

    /**
     * This method gets the country of the Client from the altdb
     * @return the country of the Client is being return
     */
    public String getCountry() {
        return country;
    }

    /**
     * This method sets the country of the Client from the form
     * @param country the country of Client is set in the altdb
     */
    public void setCountry(String country) {
        this.country = country;
    }

    /**
     * This method gets the state of Client if they are active from the altdb
     * @return the active state of the Client
     */
    public int getActive() {
        return active;
    }

    /**
     * This method sets the status of the Client if they are active or not
     * @param active the active state is set in the altdb
     */
    public void setActive(int active) {
        this.active = active;
    }

    /**
     * This method provides default syntax that will be used from the altdb
     * @return this returns idclient, name, email, haircolor, pcode, st_pv, country and active
     */
    @Override
    public String toString() {
        return name;
    }
}

