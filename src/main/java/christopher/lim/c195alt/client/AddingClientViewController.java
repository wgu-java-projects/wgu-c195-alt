package christopher.lim.c195alt.client;

import christopher.lim.c195alt.helper.Utilities;
import christopher.lim.c195alt.appointment.CountryModel;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.control.CheckBox;
import javafx.scene.control.ComboBox;
import javafx.scene.control.TextField;

import java.io.IOException;
import java.net.URL;
import java.sql.SQLException;
import java.util.ResourceBundle;

/**
 * This controller class is for the Add Client View that will be used
 * to add new Client to the altdb. And it will add the new record
 * to the Client Table View.
 * @author Christopher Lim
 */
public class AddingClientViewController implements Initializable {
    // These are the list of fx:id that will be used for the adding client view
    @FXML private TextField addClientViewClientName;
    @FXML private TextField addClientViewClientEmail;
    @FXML private TextField addClientViewHairColor;
    @FXML private TextField addClientViewPostalCode;
    @FXML private TextField addClientViewStateStateProvince;
    @FXML private ComboBox<CountryModel> addClientViewClientCountry;
    @FXML private CheckBox addClientViewActive;

    // This field will be used to check if the Client is active or not
    int activeStatus = 0;

    /**
     * This method is being used by the Active Checkbox, in which if the checkbox is selected
     * it will return the value of 1. And if its unchecked, it will return the value of 0.
     * @param actionEvent the action event will be triggered once the active checkbox is selected
     * @throws IOException will be triggered if I/O exception to some sort has occurred.
     */
    @FXML
    public void setActiveCheckbox(ActionEvent actionEvent) throws IOException {
        if (addClientViewActive.isSelected()) {
            Alert alert = new Alert(Alert.AlertType.INFORMATION);
            alert.setTitle("Information");
            alert.setHeaderText("Client's Active Status");
            alert.setContentText("You just make the client Active.");
            alert.showAndWait();

           activeStatus = 1;
        } else {
            activeStatus = 0;
        }
    }



    /**
     * This method will add a new Client to the Client Table
     * @param actionEvent this action event will be triggered after the Adding Client Button is clicked
     * @throws IOException will be triggered if I/O exception to some sort has occurred.
     */
    @FXML
    public void onActionAddingClientButton(ActionEvent actionEvent) throws IOException {

        // The fields for grabbing the data from the text field
        String addClientViewClientNameText = addClientViewClientName.getText();
        String addClientViewClientEmailText = addClientViewClientEmail.getText();
        String addClientViewHairColorText = addClientViewHairColor.getText();
        String addClientViewPostalCodeText = addClientViewPostalCode.getText();
        String addClientViewStateStateProvinceText = addClientViewStateStateProvince.getText();

        // This if statement will check if the value of the fields are empty.
        // If they are empty, an alert message will appear that the fields must be filled.
        if (addClientViewClientNameText == "" || addClientViewClientEmailText == "" ||
                addClientViewHairColorText == "" || addClientViewPostalCodeText =="" || addClientViewStateStateProvinceText == "") {
            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.setTitle("Error");
            alert.setHeaderText("Every field needs to be filled out!");
            alert.setContentText("Please check to see if any fields need to be filled up.");
            alert.showAndWait();
        } else {
            try {
                // This field will get the value of the country selected from the dropdown
                String countryName = addClientViewClientCountry.getSelectionModel().getSelectedItem().getCountryName();
                // This field will get the max value of the client id and increase it by 1
                int clientId = ClientSQLQuery.getClientMaxId();

                // We are creating a new Client Model object
                ClientModel clientModel = new ClientModel(clientId, addClientViewClientNameText, addClientViewClientEmailText,
                        addClientViewHairColorText, addClientViewPostalCodeText, addClientViewStateStateProvinceText, countryName, activeStatus);

                // On this line we are calling the sql method from Client DAO to insert
                // the new Client Model object to the altDB
                ClientSQLQuery.addingClientToAltDB(clientModel);
                Utilities.navigateApplicationView(actionEvent, "/christopher/lim/c195alt/view/ClientTableView.fxml");
            } catch (SQLException e) {
                throw new RuntimeException(e);
            }
        }

    }

    /**
     * This method will cancel adding the client form and will navigate back to the Client Table View
     * @param actionEvent the event will be triggered when the Cancel Adding Client is clicked
     * @throws IOException will be triggered if I/O exception to some sort has occurred.
     */
    @FXML
    public void onActionCancelAddingClientButton(ActionEvent actionEvent) throws IOException {
        Utilities.navigateApplicationView(actionEvent, "/christopher/lim/c195alt/view/ClientTableView.fxml");
    }

    /**
     * This initializes the Adding Client Table View page
     * @param url this sets the url
     * @param resourceBundle this sets the resource bundle
     */
    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        try {
            addClientViewClientCountry.setItems(ClientSQLQuery.getAllClientCountryQuery());
            addClientViewClientCountry.getSelectionModel().selectFirst();
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }

    }
}
