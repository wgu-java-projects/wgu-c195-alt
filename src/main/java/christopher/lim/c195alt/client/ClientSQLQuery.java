package christopher.lim.c195alt.client;

import static christopher.lim.c195alt.helper.JavaDataBaseConnectivity.connection;

import christopher.lim.c195alt.appointment.CountryModel;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * This class will handle the Client query from the altdb.
 * @author Christopher Lim
 */
public class ClientSQLQuery {

    /**
     * This method gets all the Country from the client table by filtering
     * all unique Country.
     *
     * @return the selectAllCountryObservationList will be returned with unique Country
     * @throws SQLException this will give out information about database access error
     */
    public static ObservableList<CountryModel> getAllClientCountryQuery() throws SQLException {
        // These are fields that will be used in query
        ObservableList<CountryModel> selectAllCountryObservationList = FXCollections.observableArrayList();
        String sql = "SELECT DISTINCT country FROM altdb.client";

        PreparedStatement sqlStatement = connection.prepareStatement(sql);
        ResultSet resultSet = sqlStatement.executeQuery();
        while (resultSet.next()) {
            String country = resultSet.getString("country");
            CountryModel countryModel = new CountryModel(country);
            selectAllCountryObservationList.add(countryModel);
        }

        return selectAllCountryObservationList;
    }

    /**
     * This method will query the Client table with the current
     * Max client id. Then after finding the number, we grab the value
     * and add one to increment the number.
     * @return It will return idclient + 1 if the query found the max value.
     * If not, it will return a value of 1.
     * @throws SQLException this will give out information about database access error
     */
    public static int getClientMaxId() throws SQLException {
        // These are fields that will be used in query
        String sql = "SELECT MAX(idclient) FROM altdb.client;";

        try (PreparedStatement sqlStatement = connection.prepareStatement(sql);) {
            ResultSet resultSet = sqlStatement.executeQuery();
            if (resultSet.next()) {
                System.out.println("Max client id: " + resultSet.getInt("MAX(idclient)"));
                return resultSet.getInt("MAX(idclient)") + 1;
            }
        } catch (SQLException e) {
            System.out.println(e);
        }

        return 1;
    }

    /**
     * This method will insert the new Client to the altdb and it will create a new table row
     * in the Client Table View.
     * @param clientModel the client model is being used to access the getter
     * @return the addNewClientRow is returning the new Client Model
     * @throws SQLException this will give out information about database access error
     */
    public static boolean addingClientToAltDB(ClientModel clientModel) throws SQLException {
        // These are fields that will be used in query
        int addNewClientRow = 0;
        String sql = "INSERT INTO altdb.client(idclient, name, email, haircolor, pcode, st_pv, country, Active) VALUES(?,?,?,?,?,?,?,?)";

        try (PreparedStatement sqlStatement = connection.prepareStatement(sql)) {
            sqlStatement.setInt(1, clientModel.getIdclient());
            sqlStatement.setString(2, clientModel.getName());
            sqlStatement.setString(3, clientModel.getEmail());
            sqlStatement.setString(4, clientModel.getHaircolor());
            sqlStatement.setString(5, clientModel.getPcode());
            sqlStatement.setString(6, clientModel.getSt_pv());
            sqlStatement.setString(7, clientModel.getCountry());
            sqlStatement.setInt(8, clientModel.getActive());

            addNewClientRow = sqlStatement.executeUpdate();

            System.out.println(addNewClientRow);
        } catch (SQLException e) {
            System.out.println("Method addingClientToAltDB: " + e);
            throw e;
        }

        return addNewClientRow > 0;
    }

    /**
     * This method will run a query in the altdb to update the Client by using the client's id.
     * @param clientModel is the object that will be passed when update the Client Object
     * @return It will return the updatingClientRow with the updated data.
     * @throws SQLException this will give out information about database access error
     */
    public static boolean updatingClientFromAltDB(ClientModel clientModel) throws SQLException {
        // These are fields that will be used in query
        int updatingClientRow = 0;
        String sql = "UPDATE altdb.client SET name = ?, email = ?, haircolor = ?, pcode = ?, st_pv = ?, country = ?, Active = ? WHERE idclient = ?";

        try (PreparedStatement sqlStatement = connection.prepareStatement(sql)) {
            sqlStatement.setString(1, clientModel.getName());
            sqlStatement.setString(2, clientModel.getEmail());
            sqlStatement.setString(3, clientModel.getHaircolor());
            sqlStatement.setString(4, clientModel.getPcode());
            sqlStatement.setString(5, clientModel.getSt_pv());
            sqlStatement.setString(6, clientModel.getCountry());
            sqlStatement.setInt(7, clientModel.getActive());
            sqlStatement.setInt(8, clientModel.getIdclient());

            updatingClientRow = sqlStatement.executeUpdate();

            System.out.println(updatingClientRow);
        } catch (SQLException e) {
            System.out.println("Method updatingClientFromAltDB: " + e);
            throw e;
        }

        return updatingClientRow > 0;
    }

    /**
     * This method will run the delete query in the altdb for a specific
     * Client Id.
     * @param deleteClientId the delete client id is the id of the Client
     * we want to delete
     * @return the deleteClientRow will return the deleted client id
     * @throws SQLException this will give out information about database access error
     */
    public static boolean deletingClientFromAltDB(int deleteClientId) throws SQLException {
        // These are fields that will be used in query
        int deleteClientRow = 0;
        String sql = "DELETE FROM altdb.client WHERE idclient = ?";

        try (PreparedStatement sqlStatement = connection.prepareStatement(sql)){
            sqlStatement.setInt(1, deleteClientId);
            deleteClientRow = sqlStatement.executeUpdate();
            System.out.println(deleteClientRow);
        } catch (SQLException e){
            System.out.println("Method deletingClientFromAltDB: " + e);
            throw e;
        }

        return deleteClientRow > 0;
    }


    /**
     * This method will run the query in the altdb to get all the client.
     * After that, it will add the result to the selectAllClientObservationList.
     * In which will return the list of Client and be displayed on the table.
     * @return the selectAllClientObservationList will be returned.
     * @throws SQLException this will give out information about database access error
     */
    public static ObservableList<ClientModel> getAllClientQuery() throws SQLException {
        // These are fields that will be used in query
        ObservableList<ClientModel> selectAllClientObservationList = FXCollections.observableArrayList();
        String sql = "SELECT * FROM altdb.client";

        PreparedStatement sqlStatement = connection.prepareStatement(sql);
        ResultSet resultSet = sqlStatement.executeQuery();
        while (resultSet.next()) {
            selectAllClientObservationList.add(buildClientFromResultSet(resultSet));
        }

        return selectAllClientObservationList;
    }

    /**
     * This method will build the Client by grabbing the Client's id, name, email, hair color,
     * postal code, state/province, country and Active
     * @param resultSet will set the result set of the sql query
     * @return this will return the Client Model Client's id, name, email, hair color,
     * postal code, state/province, country and Active
     * @throws SQLException this will give out information about database access error
     */
    public static ClientModel buildClientFromResultSet(ResultSet resultSet) throws SQLException {
        // These are fields that will get the client's id, name, email, hair color, postal code, state/province
        // country, and active
        int idclient = resultSet.getInt("idclient");
        String name = resultSet.getString("name");
        String email = resultSet.getString("email");
        String haircolor = resultSet.getString("haircolor");
        String pcode = resultSet.getString("pcode");
        String pt_pv = resultSet.getString("st_pv");
        String country = resultSet.getString("country");
        int active = resultSet.getInt("Active");

        System.out.println("Client Id: " + idclient + ", Name: " + name + ", Email: " + email + ", Hair color: " + haircolor + ", Postal Code: " + pcode + ", State/Province: " + pt_pv + ", Country: " + country + ", Active: " + active);
        return new ClientModel(idclient, name, email, haircolor, pcode, pt_pv, country, active);
    }
}
