package christopher.lim.c195alt.login;

/**
 * A class to create the User object in the altdb
 * @author Christopher Lim
 */
public class UserModel {
    private int idUser;
    private String loginName;
    private String psw;

    /**
     * This is the class constructor for the User Model
     * @param idUser the id user is being used to uniquely identify the user
     * @param loginName the login name is being used as the username to login
     * @param psw the psw or password is being used to help protect the login
     *  with different combination of characters
     */
    public UserModel(int idUser, String loginName, String psw) {
        this.idUser = idUser;
        this.loginName = loginName;
        this.psw = psw;
    }

    /**
     * This int method will get the user identification from the altdb
     * @return it returns the idUser when it is called
     */
    public int getIdUser() {
        return idUser;
    }

    /**
     * This method is being used to set the id of the user in the altdb
     * @param idUser the param needs to be an integer
     */
    public void setIdUser(int idUser) {
        this.idUser = idUser;
    }

    /**
     * This method gets the login name of the user from the altdb
     * @return the loginName is being returned when the method is called
     */
    public String getLoginName() {
        return loginName;
    }

    /**
     * This method sets the login name of the user from the altdb
     * @param loginName the param needs to be a string
     */
    public void setLoginName(String loginName) {
        this.loginName = loginName;
    }

    /**
     * This method return the psw or password from the altdb
     * @return the psw or password is being returned
     */
    public String getPsw() {
        return psw;
    }

    /**
     * This method sets the psw or password from the altdb
     * @param psw the param needs to be a string
     */
    public void setPsw(String psw) {
        this.psw = psw;
    }

    /**
     * This method provides default syntax that will be used from the altdb
     * @return this returns the idUser and loginName
     */
    @Override
    public String toString() {
        return ("User Id: [" + idUser + "] | Username: [" + loginName + "] | Password: [" + psw + "] ");
    }
}
