package christopher.lim.c195alt.login;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Optional;

import static christopher.lim.c195alt.helper.JavaDataBaseConnectivity.connection;

/**
 * This class will handle the User query from the altdb.
 * @author Christopher Lim
 */
public class UserSQLQuery {

    /**
     * This method will do a validation query from the altdb. If it's successful,
     * the User can log in. If they are not, they stayed in the Login View.
     * @param username this is the username of the User
     * @param password this is the password of the User
     * @return it will return buildUserFromResultSet if the username and password exist in the
     * database. If not, it will return empty or null
     */
    public static Optional<UserModel> userLoginQuery(String username, String password) {
        String sql = "SELECT * \n" +
                "FROM altdb.user\n" +
                "WHERE loginName = ? AND psw = ? ";

        try (PreparedStatement sqlStatement = connection.prepareStatement(sql)) {
            sqlStatement.setString(1, username);
            sqlStatement.setString(2, password);

            try (ResultSet resultSet = sqlStatement.executeQuery()){
                if (resultSet.next()) {
                    return Optional.of(buildUserFromResultSet(resultSet));
                } else {
                    return Optional.empty();
                }
            }

        } catch (SQLException e) {
            System.out.println(e);
        }
        return Optional.empty();
    }

    /**
     * This method will build the User by grabbing the user's id, login name and password
     * @param resultSet will set the result set of the sql query
     * @return this will return the User Model that has idUser, loginName, psw
     * @throws SQLException this will give out information about database access error
     */
    public static UserModel buildUserFromResultSet(ResultSet resultSet) throws SQLException {
        int idUser = resultSet.getInt("idUser");
        String loginName = resultSet.getString("loginName");
        String psw = resultSet.getString("psw");

        System.out.println("User id: " + idUser + ", Username: " + loginName + ", Password: " + psw);
        return new UserModel(idUser, loginName, psw);
    }
}
