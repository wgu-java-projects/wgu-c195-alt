package christopher.lim.c195alt.login;

import christopher.lim.c195alt.appointment.AppointmentModel;
import christopher.lim.c195alt.appointment.AppointmentSQLQuery;
import christopher.lim.c195alt.helper.Utilities;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.*;

import java.io.IOException;
import java.net.URL;
import java.sql.SQLException;
import java.time.LocalDateTime;
import java.util.*;
import java.util.stream.Stream;


/**
 * This controller class is for the LoginView that will be used
 * to log in the User to the Salon Appointment App
 * @author Christopher Lim
 */
public class LoginViewController implements Initializable {
    // This is the String array for languages that will be used for changing the language
    private final String[] setEnglishLanguages = {"English" , "Spanish"};
    private final String[] setSpanishLanguages = {"Inglés" , "Español"};

    // The currentUserObject field will grab the UserModel toString method
    public static UserModel currentUserObject;

    // These are the list of fx:id that will be used for the login view
    @FXML private Label loginViewTitleLabel;
    @FXML private Label loginViewUsernameLabel;
    @FXML private TextField loginViewUsernameTextField;
    @FXML private Label loginViewUsernameErrorLabel;
    @FXML private Label loginViewPasswordLabel;
    @FXML private TextField loginViewPasswordTextField;
    @FXML private Label loginViewPasswordErrorLabel;
    @FXML private Button loginViewLoginButton;
    @FXML private Button loginViewExitButton;
    @FXML private Label loginViewTimeZoneLabel;
    @FXML private Label loginViewLanguageLabel;
    @FXML private ChoiceBox<String> loginViewLanguageChoiceBox;


    /**
     * This method will check if the entered username and password exist
     * from the database. If it does then it will return true and if not
     * it will return false.
     * @param username the username text will be taken from the username text field of the login form
     * @param password the password text will be taken from the password text field of the login form
     * @return this will return true or false if both username and password is found or not
     */
    public static boolean authZeroLogin(String username, String password) {
        // This will access the UserSQLQuery and use the userLoginQuery method and pass the
        // username and password parameters and grab the result and pass it to
        // the getCurrentUserResult field.
        Optional<UserModel> getCurrentUserResult = UserSQLQuery.userLoginQuery(username, password);
        // This if and else statement checks if the username and password is found
        if(getCurrentUserResult.isPresent()) {
            currentUserObject = getCurrentUserResult.get();
            System.out.println("Testing: " + currentUserObject);
            return true;
        } else {
            return false;
        }

    }

    /**
     * This method will check for the username and password field if they are empty or blank.
     * LAMBDA EXPRESSION: character -> character.isBlank() || character.isEmpty()
     * returns true if the String is blank or empty. If it returns false, it will go to another
     * if checks for the login
     * @return it will return  true or false depending on if the fields are empty or not
     */
    public boolean checkUserNamePassword() {
        return Stream.of(loginViewUsernameTextField.getText(), loginViewPasswordTextField.getText()).allMatch(character -> character.isBlank() || character.isEmpty());
    }

    /**
     * This method will first check if the username and password exist in the database.
     * If it does exist, it will log in the user to the Appointment View table.And if the
     * username or password is either empty or invalid. It will keep the user on the login view screen.
     * Also, there will be a log that will write to the log.txt file if the user has successful or not.
     * The log.txt will have login attempts, timestamp and if the login is successful.
     * @param actionEvent the action event is used for navigating the next screen.
     * @throws IOException will be triggered if I/O exception to some sort has occurred.
     */
    @FXML
    public void onActionLoginButton(ActionEvent actionEvent) throws IOException, SQLException {
        ObservableList<AppointmentModel> getAllAppointments = AppointmentSQLQuery.getAllAppointmentQuery();
        LocalDateTime before15Min = LocalDateTime.now().minusMinutes(15);
        LocalDateTime after15Min = LocalDateTime.now().plusMinutes(15);
        LocalDateTime appointmentStartTime;

        int appointmentId = 0;
        String appointmentName = "";
        LocalDateTime currentAppointmentTime = null;
        boolean appointmentWithin15Min = false;

        String username = loginViewUsernameTextField.getText();
        String password = loginViewPasswordTextField.getText();

        // This will check the username and password are correct from the database
        if (authZeroLogin(username, password)) {
            Utilities.navigateApplicationView(actionEvent, "/christopher/lim/c195alt/view/ClientTableView.fxml");
            Utilities.loginActivityLog(username,password, "Login is Successful!");

            //check for upcoming appointments if user is validated
            for (AppointmentModel appointment: getAllAppointments) {
                appointmentStartTime = appointment.getStart();
                if ((appointmentStartTime.isAfter(before15Min) || appointmentStartTime.isEqual(before15Min)) && (appointmentStartTime.isBefore(after15Min) || (appointmentStartTime.isEqual(after15Min)))) {
                    appointmentId = appointment.getIdappt();
                    appointmentName = appointment.getTitle();
                    currentAppointmentTime = appointmentStartTime;
                    appointmentWithin15Min = true;
                }
            }

            if (appointmentWithin15Min) {
                Alert alert = new Alert(Alert.AlertType.INFORMATION);
                alert.setTitle("Salon Appointment Information");
                alert.setHeaderText("Appointment Reminder");
                alert.setContentText("You have an appointment within 15 minutes.\n" +
                        "Appointment ID: " +  appointmentId
                        + "\nAppointment Name: " + appointmentName
                        + "\nAppointment Time: " + currentAppointmentTime);
                alert.showAndWait();

            } else {
                Alert alert = new Alert(Alert.AlertType.INFORMATION);
                alert.setTitle("Salon Appointment Information");
                alert.setHeaderText("Appointment Reminder");
                alert.setContentText("You have no appointment within 15 minutes." );
                alert.showAndWait();
            }

            // This will use the lambda expression method
        } else if (checkUserNamePassword()){
            Utilities.loginActivityLog(username,password, "Login is Unsuccessful!");
            loginViewUsernameErrorLabel.setVisible(true);
            loginViewPasswordErrorLabel.setVisible(true);
            // This will check if both username and password is wrong
        } else if ((!Objects.equals(username, "altUser")) & (!Objects.equals(password, "altPSW"))) {
            Utilities.loginActivityLog(username,password, "Login is Unsuccessful!");
            loginViewUsernameErrorLabel.setVisible(true);
            loginViewPasswordErrorLabel.setVisible(true);
            // This will check if the username is correct but the password is wrong
        } else if ((Objects.equals(username, "altUser")) & (!Objects.equals(password, "altPSW"))) {
            Utilities.loginActivityLog(username,password, "Login is Unsuccessful!");
            loginViewUsernameErrorLabel.setVisible(false);
            loginViewPasswordErrorLabel.setVisible(true);
            // This will check if the username is wrong but the password is correct
        } else if ((!Objects.equals(username, "altUser")) & (Objects.equals(password, "altPSW"))) {
            Utilities.loginActivityLog(username,password, "Login is Unsuccessful!");
            loginViewUsernameErrorLabel.setVisible(true);
            loginViewPasswordErrorLabel.setVisible(false);
        }

    }

    /**
     * This method will close the whole application.
     * @param actionEvent the action event is being triggered when the exit button is clicked.
     * @throws IOException will be triggered if I/O exception to some sort has occurred.
     */
    @FXML
    public void onActionExitButton(ActionEvent actionEvent) throws IOException {
        System.exit(0);
    }

    /**
     * This method is used for selecting which language the user wants to use.
     * @param actionEvent the action event is being triggered when the ChoiceBox
     * is being clicked.
     */
    @FXML
    public void onActionLanguageChoiceBox(ActionEvent actionEvent) {
        String currentLanguage = loginViewLanguageChoiceBox.getValue();
        if (currentLanguage.equals("Spanish") || currentLanguage.equals("Español")){
            setLanguageEs();
        }
        else {
            setLanguageEn();
        }
    }

    /**
     * This method sets the login screen or page to the English Language.
     */
    public void setLanguageEn() {
        // This set the default locale to en or english
        Locale.setDefault(new Locale("en"));

        // This is grabbing the resource bundle for /lang_en file
        ResourceBundle resourceBundle = ResourceBundle.getBundle("/lang_en", Locale.getDefault());

        // This will set up the label of the login form the english keys
        loginViewTitleLabel.setText(resourceBundle.getString("loginViewTitleLabel"));
        loginViewUsernameLabel.setText(resourceBundle.getString("loginViewUsernameLabel"));
        loginViewUsernameTextField.setPromptText(resourceBundle.getString("loginViewUsernameTextField"));
        loginViewUsernameErrorLabel.setText(resourceBundle.getString("loginViewUsernameErrorLabel"));
        loginViewPasswordLabel.setText(resourceBundle.getString("loginViewPasswordLabel"));
        loginViewPasswordTextField.setPromptText(resourceBundle.getString("loginViewPasswordTextField"));
        loginViewPasswordErrorLabel.setText(resourceBundle.getString("loginViewPasswordErrorLabel"));
        loginViewLoginButton.setText(resourceBundle.getString("loginViewLoginButton"));
        loginViewExitButton.setText(resourceBundle.getString("loginViewExitButton"));
        loginViewTimeZoneLabel.setText("Time Zone: " + Calendar.getInstance().getTimeZone().getID());
        loginViewLanguageLabel.setText(resourceBundle.getString("loginViewLanguageLabel"));
    }

    /**
     * This method sets the login screen or page to the Spanish Language.
     */
    public void setLanguageEs() {
        // This set the default locale to es or spanish
        Locale.setDefault(new Locale("es"));

        // This is grabbing the resource bundle for /lang_es file
        ResourceBundle resourceBundle = ResourceBundle.getBundle("/lang_es", Locale.getDefault());

        //This will set up the label of the login form the spanish keys
        loginViewTitleLabel.setText(resourceBundle.getString("loginViewTitleLabel"));
        loginViewUsernameLabel.setText(resourceBundle.getString("loginViewUsernameLabel"));
        loginViewUsernameTextField.setPromptText(resourceBundle.getString("loginViewUsernameTextField"));
        loginViewUsernameErrorLabel.setText(resourceBundle.getString("loginViewUsernameErrorLabel"));
        loginViewPasswordLabel.setText(resourceBundle.getString("loginViewPasswordLabel"));
        loginViewPasswordTextField.setPromptText(resourceBundle.getString("loginViewPasswordTextField"));
        loginViewPasswordErrorLabel.setText(resourceBundle.getString("loginViewPasswordErrorLabel"));
        loginViewLoginButton.setText(resourceBundle.getString("loginViewLoginButton"));
        loginViewExitButton.setText(resourceBundle.getString("loginViewExitButton"));
        loginViewTimeZoneLabel.setText("Zona Horaria: " + Calendar.getInstance().getTimeZone().getID());
        loginViewLanguageLabel.setText(resourceBundle.getString("loginViewLanguageLabel"));
    }

    /**
     * This initializes the Login View page
     * @param url this sets the url
     * @param resourceBundle this sets the resource bundle
     */
    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        // This sets the error labels hidden
        loginViewUsernameErrorLabel.setVisible(false);
        loginViewPasswordErrorLabel.setVisible(false);


        // This sets the languages default language
        Locale currentLocale = Locale.getDefault();
        System.out.println("Current Default Locale: " + currentLocale);
        Locale.setDefault(currentLocale);
        
        if (currentLocale.toString().equals("en_US") || currentLocale.toString().equals("en") ){
            loginViewLanguageChoiceBox.getItems().addAll(setEnglishLanguages);
            loginViewLanguageChoiceBox.setValue("English");
            setLanguageEn();
            loginViewLanguageChoiceBox.setOnAction(this::onActionLanguageChoiceBox);
        } else if (currentLocale.toString().equals("es_US")) {
            loginViewLanguageChoiceBox.getItems().addAll(setSpanishLanguages);
            loginViewLanguageChoiceBox.setValue("Español");
            setLanguageEs();
            loginViewLanguageChoiceBox.setOnAction(this::onActionLanguageChoiceBox);
        }
    }
}
