package christopher.lim.c195alt.report;

import christopher.lim.c195alt.appointment.AppointmentModel;
import christopher.lim.c195alt.appointment.StylistModel;
import christopher.lim.c195alt.appointment.StylistSQLQuery;
import christopher.lim.c195alt.client.ClientTableViewController;
import christopher.lim.c195alt.helper.Utilities;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;

import java.io.IOException;
import java.net.URL;
import java.sql.SQLException;
import java.time.DayOfWeek;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.temporal.TemporalAdjusters;
import java.util.ResourceBundle;

/**
 * This controller class is for the Report Table View that will be used
 * to display all the appointments in a table from the altdb. And it will let the user
 * filter the appointments by week, type, stylist and country.
 * @author Christopher Lim
 */
public class ReportTableViewController implements Initializable {
    // These are the list of fx:id that will be used for the adding report view
    @FXML private ComboBox<TypeModel> reportViewType;
    @FXML private ComboBox<StylistModel> reportViewStylist;
    @FXML private ComboBox<LocationModel> reportViewLocation;
    @FXML private TableView<AppointmentModel> reportViewTableViewTableView;
    @FXML private TableColumn<AppointmentModel, Integer> reportViewAppointmentIdColumn;
    @FXML private TableColumn<AppointmentModel, String> reportViewTitleColumn;
    @FXML private TableColumn<AppointmentModel, String> reportViewDescriptionColumn;
    @FXML private TableColumn<AppointmentModel, String> reportViewLocationColumn;
    @FXML private TableColumn<AppointmentModel, String> reportViewTypeColumn;
    @FXML private TableColumn<AppointmentModel, LocalDateTime> reportViewStartColumn;
    @FXML private TableColumn<AppointmentModel, LocalDateTime> reportViewEndColumn;
    @FXML private TableColumn<AppointmentModel, Integer> reportViewClientIdColumn;
    @FXML private TableColumn<AppointmentModel, Integer> reportViewStylistIdColumn;
    @FXML private Label reportViewTotalAppointment;

    /**
     * This method will navigate the User to the Client Table View.
     * This table will have client id, client name, client email, hair color, postal code, state/province,
     * country and Active.
     * @param actionEvent the action will be triggered when the Client Table View button is clicked.
     * @throws IOException will be triggered if I/O exception to some sort has occurred.
     */
    @FXML
    public void onActionClientTableViewButton(ActionEvent actionEvent) throws IOException {
        Utilities.navigateApplicationView(actionEvent, "/christopher/lim/c195alt/view/ClientTableView.fxml");
    }

    /**
     * This method will navigate the User to the Appointment Table View.
     * This table will have appointment id, title, description, location, type, start date and time,
     * end date and time, client id and style id.
     * @param actionEvent the action will be triggered when the Appointment Table View button is clicked.
     * @throws IOException will be triggered if I/O exception to some sort has occurred.
     */
    @FXML
    public void onActionAppointmentTableViewButton(ActionEvent actionEvent) throws IOException {
        Utilities.navigateApplicationView(actionEvent, "/christopher/lim/c195alt/view/AppointmentTableView.fxml");
    }


    /**
     * This method will log out the User and navigate them back to the Login View
     * @param actionEvent the action event is being triggered when the Log out button is clicked.
     * @throws IOException will be triggered if I/O exception to some sort has occurred.
     */
    @FXML
    public void onActionLogoutButton(ActionEvent actionEvent) throws IOException {
        Utilities.navigateApplicationView(actionEvent, "/christopher/lim/c195alt/view/LoginView.fxml");
    }

    /**
     * This method will grab the selected style from the combo box and call the SQL
     * method sortingAppointmentByWeekType and put the query information in the observableList.
     * After that we have an if statement that will check if the list we are returning is empty or not.
     * If it is empty, we will return an alert informing the user that we have no result.
     * And if there is a result, we will populate the Report Appointment Table.
     * @param actionEvent the action event will be triggered once we selected a Type of service.
     */
    @FXML
    public void onActionSelectType(ActionEvent actionEvent) {
        ObservableList<AppointmentModel> observableList;
        String selectedType = reportViewType.getSelectionModel().getSelectedItem().getType();

        LocalDate currentDay = LocalDate.now();
        LocalDate startDay = currentDay.with(TemporalAdjusters.previousOrSame(DayOfWeek.SUNDAY));
        LocalDate endDay = currentDay.with(TemporalAdjusters.nextOrSame(DayOfWeek.SATURDAY));

        observableList = ReportSQLQuery.sortingAppointmentByWeekType(startDay, endDay, selectedType);
        System.out.println(observableList);

        if (observableList.isEmpty()) {
            Alert alert = new Alert(Alert.AlertType.INFORMATION);
            alert.setTitle("Appointment");
            alert.setHeaderText("Appointment Record");
            alert.setContentText("There are no appointment with the current week and type.");
            alert.showAndWait();
            System.out.println("There are no appointment with the current week and type.");
        }

        reportViewStylist.setPromptText("Select Stylist");
        reportViewLocation.setPromptText("Select Location");
        reportViewTableViewTableView.setItems(observableList);

        int totalAppointments = reportViewTableViewTableView.getItems().size();

        if (totalAppointments == 0) {
            reportViewTotalAppointment.setText("Total Appointments: 0");
        } else {
            reportViewTotalAppointment.setText("Total Appointments: " + totalAppointments);
        }
    }

    @FXML
    public void onActionSelectStyle(ActionEvent actionEvent) {
        ObservableList<AppointmentModel> observableList;
        int selectedType = reportViewStylist.getSelectionModel().getSelectedItem().getIdstylist();

        observableList = ReportSQLQuery.sortingAppointmentByStylist(selectedType);
        System.out.println(observableList);

        if (observableList.isEmpty()) {
            Alert alert = new Alert(Alert.AlertType.INFORMATION);
            alert.setTitle("Appointment");
            alert.setHeaderText("Appointment Record");
            alert.setContentText("There are no appointment with the Stylist.");
            alert.showAndWait();
            System.out.println("There are no appointment with the Stylist.");
        }

        reportViewType.setPromptText("Select Type (Current Week)");
        reportViewLocation.setPromptText("Select Location");
        reportViewTableViewTableView.setItems(observableList);

        int totalAppointments = reportViewTableViewTableView.getItems().size();

        if (totalAppointments == 0) {
            reportViewTotalAppointment.setText("Total Appointments: 0");
        } else {
            reportViewTotalAppointment.setText("Total Appointments: " + totalAppointments);
        }
    }

    @FXML
    public void onActionSelectLocation(ActionEvent actionEvent) {
        ObservableList<AppointmentModel> observableList;
        String selectedLocation = reportViewLocation.getSelectionModel().getSelectedItem().getLocation();

        observableList = ReportSQLQuery.sortingAppointmentByLocation(selectedLocation);
        System.out.println(observableList);

        if (observableList.isEmpty()) {
            Alert alert = new Alert(Alert.AlertType.INFORMATION);
            alert.setTitle("Appointment");
            alert.setHeaderText("Appointment Record");
            alert.setContentText("There are no appointment with the Location.");
            alert.showAndWait();
            System.out.println("There are no appointment with the Location.");
        }

        reportViewType.setPromptText("Select Type (Current Week)");
        reportViewStylist.setPromptText("Select Stylist");
        reportViewTableViewTableView.setItems(observableList);

        int totalAppointments = reportViewTableViewTableView.getItems().size();

        if (totalAppointments == 0) {
            reportViewTotalAppointment.setText("Total Appointments: 0");
        } else {
            reportViewTotalAppointment.setText("Total Appointments: " + totalAppointments);
        }
    }

    /**
     * This method will close the whole application.
     * @param actionEvent the action event is being triggered when the exit button is clicked.
     * @throws IOException will be triggered if I/O exception to some sort has occurred.
     */
    @FXML
    public void onActionExitButton(ActionEvent actionEvent) throws IOException {
        System.exit(0);
    }

    /**
     * This initializes the Report Table View page
     * @param url this sets the url
     * @param resourceBundle this sets the resource bundle
     */
    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        try {
            reportViewType.setItems(ReportSQLQuery.getAllTypeQuery());
            reportViewStylist.setItems(StylistSQLQuery.getAllStylistQuery());
            reportViewLocation.setItems(ReportSQLQuery.getAllLocationQuery());
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }

        // This sets the cell value of the table for Appointments in the reports view
        reportViewAppointmentIdColumn.setCellValueFactory(new PropertyValueFactory<AppointmentModel, Integer>("idappt"));
        reportViewTitleColumn.setCellValueFactory(new PropertyValueFactory<AppointmentModel, String>("title"));
        reportViewDescriptionColumn.setCellValueFactory(new PropertyValueFactory<AppointmentModel, String>("descr"));
        reportViewLocationColumn.setCellValueFactory(new PropertyValueFactory<AppointmentModel, String>("location"));
        reportViewTypeColumn.setCellValueFactory(new PropertyValueFactory<AppointmentModel, String>("type"));
        reportViewStartColumn.setCellValueFactory(new PropertyValueFactory<AppointmentModel, LocalDateTime>("start"));
        reportViewEndColumn.setCellValueFactory(new PropertyValueFactory<AppointmentModel, LocalDateTime>("end"));
        reportViewClientIdColumn.setCellValueFactory(new PropertyValueFactory<AppointmentModel, Integer>("clientid"));
        reportViewStylistIdColumn.setCellValueFactory(new PropertyValueFactory<AppointmentModel, Integer>("stylid"));
    }
}
