package christopher.lim.c195alt.report;

/**
 * A class to create the Type object from the Appointment Table in the altdb
 * @author Christopher Lim
 */
public class TypeModel {
    private String type;

    /**
     * This is the class constructor for the Type Model
     * @param type this will pass the Type Model type
     */
    public TypeModel(String type) {
        this.type = type;
    }

    /**
     * This method will get the type from the appointment table
     * @return it will return the type
     */
    public String getType() {
        return type;
    }

    /**
     * This method will set the type from the appointment table
     * @param type the type will be set in the appointment table
     */
    public void setType(String type) {
        this.type = type;
    }

    /**
     * This method provides default syntax that will be used from the altdb
     * @return this return the appointment type
     */
    @Override
    public String toString() { return type; }
}
