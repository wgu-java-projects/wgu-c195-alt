package christopher.lim.c195alt.report;

import christopher.lim.c195alt.appointment.AppointmentModel;
import christopher.lim.c195alt.appointment.AppointmentSQLQuery;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

import static christopher.lim.c195alt.helper.JavaDataBaseConnectivity.connection;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.time.LocalDate;

/**
 * This class will handle the Report query from the altdb.
 * @author Christopher Lim
 */
public class ReportSQLQuery {

    /**
     * This method will do a query to sort the appointment by location.
     * After finding the location, it will filter out the locations that
     * the user did not select. And it will only show the location that the
     * user selected.
     * @param location the location param will get the value from the Location drop down
     * @return this will return the selectAllAppointmentObservationList
     */
    public static ObservableList<AppointmentModel> sortingAppointmentByLocation(String location) {
        // These are fields that will be used in query
        ObservableList<AppointmentModel> selectAllAppointmentObservationList = FXCollections.observableArrayList();
        String sql = "SELECT * FROM altdb.appt WHERE location = ? ORDER BY idappt DESC";

        try (PreparedStatement sqlStatement = connection.prepareStatement(sql)) {
            sqlStatement.setString(1, location);
            ResultSet resultSet = sqlStatement.executeQuery();

            while (resultSet.next()) {
                selectAllAppointmentObservationList.add(AppointmentSQLQuery.buildAppointmentModelFromResultSet(resultSet));
            }

        } catch (SQLException e) {
            System.out.println("Method sortingAppointmentByLocation: " + e);
        }

        return selectAllAppointmentObservationList;
    }

    /**
     * This method will make a query that will sort by the stylist id. This method will be
     * called when the User selected a Stylist name from the dropdown. The dropdown will use
     * the stylist name and not the id.
     * @param stylistId the stylistId will be passed down to the param when the user selected a
     * stylist name.
     * @return the selectAllAppointmentObservationList will be returned
     */
    public static ObservableList<AppointmentModel> sortingAppointmentByStylist(int stylistId) {
        // These are fields that will be used in query
        ObservableList<AppointmentModel> selectAllAppointmentObservationList = FXCollections.observableArrayList();
        String sql = "SELECT * FROM altdb.appt WHERE stylid = ? ORDER BY idappt DESC";

        try (PreparedStatement sqlStatement = connection.prepareStatement(sql)) {
            sqlStatement.setInt(1, stylistId);
            ResultSet resultSet = sqlStatement.executeQuery();

            while (resultSet.next()) {
                selectAllAppointmentObservationList.add(AppointmentSQLQuery.buildAppointmentModelFromResultSet(resultSet));
            }

        } catch (SQLException e) {
            System.out.println("Method sortingAppointmentByStylist: " + e);
        }

        return selectAllAppointmentObservationList;
    }

    /**
     * This method will do a query for the current week and by appointment type. The start will indicate the
     * start date of the week, end will indicate the end date of the week. And the type which indicate the type
     * of the appointment being done.
     * @param start the start date of the week will be passed down
     * @param end the end date of the week will be passed down
     * @param type the type of the appointment will be passed down
     * @return this will return the selectAllAppointmentObservationList that has the result of the query
     */
    public static ObservableList<AppointmentModel> sortingAppointmentByWeekType(LocalDate start, LocalDate end, String type) {
        // These are fields that will be used in query
        ObservableList<AppointmentModel> selectAllAppointmentObservationList = FXCollections.observableArrayList();
        String sql = "SELECT * FROM altdb.appt WHERE DATE(start) BETWEEN ? AND ? AND type = ? ORDER BY idappt DESC";

        try (PreparedStatement sqlStatement = connection.prepareStatement(sql)) {
            sqlStatement.setTimestamp(1, Timestamp.valueOf(start.atStartOfDay()));
            sqlStatement.setTimestamp(2, Timestamp.valueOf(end.atStartOfDay()));
            sqlStatement.setString(3, type);
            ResultSet resultSet = sqlStatement.executeQuery();

            while (resultSet.next()) {
                selectAllAppointmentObservationList.add(AppointmentSQLQuery.buildAppointmentModelFromResultSet(resultSet));
            }

        } catch (SQLException e) {
            System.out.println("Method sortingAppointmentByWeekType: " + e);
        }

        return selectAllAppointmentObservationList;
    }

    /**
     * This method will run the query to find distinct type of service from the
     * appointment table. And then we will use this query to populate the type combo box
     * in the Report View Table
     * @return this will return selectAllTypeObservationList that has the list of type
     * @throws SQLException this will give out information about database access error
     */
    public static ObservableList<TypeModel> getAllTypeQuery() throws SQLException {
        // These are fields that will be used in query
        ObservableList<TypeModel> selectAllTypeObservationList = FXCollections.observableArrayList();
        String sql = "SELECT DISTINCT(type) FROM altdb.appt";

        PreparedStatement sqlStatement = connection.prepareStatement(sql);
        ResultSet resultSet = sqlStatement.executeQuery();
        while (resultSet.next()) {
            selectAllTypeObservationList.add(buildTypeFromResultSet(resultSet));
        }

        return selectAllTypeObservationList;
    }

    public static ObservableList<LocationModel> getAllLocationQuery() throws SQLException {
        // These are fields that will be used in query
        ObservableList<LocationModel> selectAllTypeObservationList = FXCollections.observableArrayList();
        String sql = "SELECT DISTINCT(location) FROM altdb.appt";

        PreparedStatement sqlStatement = connection.prepareStatement(sql);
        ResultSet resultSet = sqlStatement.executeQuery();

        while (resultSet.next()) {
            selectAllTypeObservationList.add(buildLocationFromResultSet(resultSet));
        }

        return selectAllTypeObservationList;
    }


    /**
     * This method will build the type of the service from the Type Model.
     * @param resultSet the result set will be used to get the result from the query
     * @return this will return the result of the type model
     * @throws SQLException this will give out information about database access error
     */
    public static TypeModel buildTypeFromResultSet(ResultSet resultSet) throws SQLException {
        // This fields will get the type
        String type = resultSet.getString("type");

        System.out.println("Type: " + type);
        return new TypeModel(type);
    }

    public static LocationModel buildLocationFromResultSet(ResultSet resultSet) throws SQLException {
        // This field will get the location
        String location = resultSet.getString("location");

        System.out.println("Appointment Location: " + location);
        return new LocationModel(location);
    }
}
