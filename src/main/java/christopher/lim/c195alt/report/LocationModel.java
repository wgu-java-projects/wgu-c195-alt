package christopher.lim.c195alt.report;

/**
 * A class to create the Location object from the Appointment Table in the altdb
 * @author Christopher Lim
 */
public class LocationModel {
    private String location;

    /**
     * This is the class constructor for the location Model
     * @param location this will pass the location param
     */
    public LocationModel(String location) {
        this.location = location;
    }

    /**
     * This will get the location of the appointment
     * @return this will return the location
     */
    public String getLocation() {
        return location;
    }

    /**
     * This will set the location of the appointment
     * @param location this will set the location
     */
    public void setLocation(String location) {
        this.location = location;
    }

    /**
     * This method provides default syntax that will be used from the altdb
     * @return this return the appointment location
     */
    @Override
    public String toString() { return location; }
}
