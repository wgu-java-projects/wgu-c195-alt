package christopher.lim.c195alt.appointment;

import java.time.LocalDateTime;

/**
 * A class to create the Appointment object in the altdb
 * @author Christopher Lim
 */
public class AppointmentModel {
    private int idappt;
    private String title;
    private String descr;
    private String location;
    private String type;
    private LocalDateTime start;
    private LocalDateTime end;
    private int clientid;
    private int stylid;

    /**
     * This is the class constructor for the Appointment Model
     * @param idappt the appointment id and needs to be an integer
     * @param title the title is being used for the appointments title and must be a string
     * @param descr the description is being used for the description of the appointment and must be a string
     * @param location the location is being used for the location of the appointment and must be a string
     * @param type the type is the type of the appointment and must be a string
     * @param start the start param will be used for the start date and time of the appointment
     * @param end the end param will be used for the end date and time of the appointment
     * @param clientid the client id identify which client is assigned to the appointment
     * @param stylid the style id is used to identify what hairstyle will be done with the appointment
     */
    public AppointmentModel(int idappt, String title, String descr, String location, String type, LocalDateTime start, LocalDateTime end, int clientid, int stylid) {
        this.idappt = idappt;
        this.title = title;
        this.descr = descr;
        this.location = location;
        this.type = type;
        this.start = start;
        this.end = end;
        this.clientid = clientid;
        this.stylid = stylid;
    }

    /**
     * This method will get the appointment id from the altdb
     * @return this will return the appointment id
     */
    public int getIdappt() {
        return idappt;
    }

    /**
     * This method will set the appointment id on the newly created appointment
     * @param idappt the appointment id will be unique
     */
    public void setIdappt(int idappt) {
        this.idappt = idappt;
    }

    /**
     * This method will get the title of the appointment from the altdb
     * @return this will return the appointment title
     */
    public String getTitle() {
        return title;
    }

    /**
     * This method will set the title of the new appointment
     * @param title the title will be added to the appointment
     */
    public void setTitle(String title) {
        this.title = title;
    }

    /**
     * This method will get the description of the appointment from the altdb
     * @return this will return the description of the appointment
     */
    public String getDescr() {
        return descr;
    }

    /**
     * This method will set the description for the new appointment
     * @param descr the description will be set to the appointment
     */
    public void setDescr(String descr) {
        this.descr = descr;
    }

    /**
     * This method will get the location of the appointment from the altdb
     * @return the location will be returned from the appointment
     */
    public String getLocation() {
        return location;
    }

    /**
     * This method will set the location for the new appointments
     * @param location the location will be set the location of the appointment
     */
    public void setLocation(String location) {
        this.location = location;
    }

    /**
     * This method will get the type of the appointment from the altdb
     * @return the type location will be returned from the appointment
     */
    public String getType() {
        return type;
    }

    /**
     * This method will set the type of the appointment for the new appointment
     * @param type the type of the appointment will be set to the appointment
     */
    public void setType(String type) {
        this.type = type;
    }

    /**
     * This method will get the appointments start date and time from the altdb
     * @return the start date and time of the appointment will be returned
     */
    public LocalDateTime getStart() {
        return start;
    }

    /**
     * This method will set the start date and time of the new appointment
     * @param start the start date and time
     */
    public void setStart(LocalDateTime start) {
        this.start = start;
    }

    /**
     * This method will get the appointments end date and time from the altdb
     * @return the end date and time of the appointment
     */
    public LocalDateTime getEnd() {
        return end;
    }

    /**
     * This method will set the appointment end date and time for the new appointment
     * @param end the end date and time will be set to the appointment
     */
    public void setEnd(LocalDateTime end) {
        this.end = end;
    }

    /**
     * This method will get the client id in the appointment from the altdb
     * @return the client id of the appointment
     */
    public int getClientid() {
        return clientid;
    }

    /**
     * This method sets the client id for the new appointment
     * @param clientid the client id is set for the appointment
     */
    public void setClientid(int clientid) {
        this.clientid = clientid;
    }

    /**
     * This method will get the style id for the appointment from the altdb
     * @return
     */
    public int getStylid() {
        return stylid;
    }

    /**
     * This method will set the styleId for the new appointment
     * @param stylid the style id will be set for the appointment
     */
    public void setStylid(int stylid) {
        this.stylid = stylid;
    }

    /**
     * This method provides default syntax that will be used from the altdb
     * @return this returns the idappt, title, descr, location, type, start,
     * end, clientid and stylid
     */
    @Override
    public String toString() {
        return ("Appointment: [" + idappt + "] | Title: [" + title + "] " +
                "| Description: [" + descr + "] | Location: " + location + "| Type: " + type
                + " | Start Date: " + start + " | End Date: " + end + " | Client Id: " + clientid + " | Style Id: " + stylid);
    }
}
