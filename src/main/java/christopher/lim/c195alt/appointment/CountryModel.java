package christopher.lim.c195alt.appointment;

/**
 * A class to create the Country object in the altdb
 * @author Christopher Lim
 */
public class CountryModel {
    private String countryName;

    /**
     * This is the class constructor for the Country Model
     * @param countryName this will pass the Country Model country name
     */
    public CountryModel(String countryName) {
        this.countryName = countryName;
    }

    /**
     * This method will get the country name from the altdb
     * @return it will return the country name
     */
    public String getCountryName() {
        return countryName;
    }

    /**
     * This method will set the country name of th Country Model
     * @param countryName the country name will be set in the altdb
     */
    public void setCountryName(String countryName) {
        this.countryName = countryName;
    }

    /**
     * This method provides default syntax that will be used from the altdb
     * @return this return the country name
     */
    @Override
    public String toString() {
        return countryName;
    }
}
