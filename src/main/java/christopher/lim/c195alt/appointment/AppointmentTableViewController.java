package christopher.lim.c195alt.appointment;

import christopher.lim.c195alt.helper.Utilities;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.stage.Stage;

import java.io.IOException;
import java.net.URL;
import java.sql.SQLException;
import java.time.DayOfWeek;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.YearMonth;
import java.time.temporal.TemporalAdjusters;
import java.util.Optional;
import java.util.ResourceBundle;

/**
 * This controller class is for the Appointment Table View that will be used
 * to display all the appointments in a table from the altdb. And it will let the user
 * Add, Edit and Delete Appointment.
 * @author Christopher Lim
 */
public class AppointmentTableViewController implements Initializable {
    // These are the list of fx:id that will be used for the client table view
    @FXML private TableView<AppointmentModel> appointmentModelTableViewTableView;
    @FXML private TableColumn<AppointmentModel, Integer> appointmentViewAppointmentIdColumn;
    @FXML private TableColumn<AppointmentModel, String> appointmentViewTitleColumn;
    @FXML private TableColumn<AppointmentModel, String> appointmentViewDescriptionColumn;
    @FXML private TableColumn<AppointmentModel, String> appointmentViewLocationColumn;
    @FXML private TableColumn<AppointmentModel, String> appointmentViewTypeColumn;
    @FXML private TableColumn<AppointmentModel, LocalDateTime> appointmentViewStartColumn;
    @FXML private TableColumn<AppointmentModel, LocalDateTime> appointmentViewEndColumn;
    @FXML private TableColumn<AppointmentModel, Integer> appointmentViewClientIdColumn;
    @FXML private TableColumn<AppointmentModel, Integer> appointmentViewStylistIdColumn;
    @FXML private ComboBox<String> appointmentViewSortBy;

    String[] sortName = { "All", "Month", "Week"};

    /**
     * This method will navigate the User to the Client Table View.
     * This table will have client id, client name, client email, hair color, postal code, state/province,
     * country and Active.
     * @param actionEvent the action will be triggered when the Client Table View button is clicked.
     * @throws IOException will be triggered if I/O exception to some sort has occurred.
     */
    @FXML
    public void onActionClientTableViewButton(ActionEvent actionEvent) throws IOException {
        Utilities.navigateApplicationView(actionEvent, "/christopher/lim/c195alt/view/ClientTableView.fxml");
    }

    /**
     * This method will navigate the User to the Report Table View.
     * It will have schedules for Stylist and clients appointments by type and week
     * @param actionEvent the action will be triggered when the Report Table View is clicked
     * @throws IOException will be triggered if I/O exception to some sort has occurred.
     */
    @FXML
    public void onActionReportTableViewButton(ActionEvent actionEvent) throws IOException {
        Utilities.navigateApplicationView(actionEvent, "/christopher/lim/c195alt/view/ReportTableView.fxml");
    }

    /**
     * This method will navigate to the Add Appointment View. In which the user can add a new appointment
     * to the appointment table view. This Add Appointment View form will have an appointment id that is
     * going to be auto generated, title, description, location and type. The start date time and end date time
     * will be broken down into two parts. The start/end date of the appointment and start/end time of the appointment.
     * Then there will be a checkbox for both existing client and stylist.
     * @param actionEvent the action event will be triggered once the adding appointment button is clicked.
     * @throws IOException will be triggered if I/O exception to some sort has occurred.
     */
    @FXML
    public void onActionAddingAppointmentButton(ActionEvent actionEvent) throws IOException {
        Utilities.navigateApplicationView(actionEvent, "/christopher/lim/c195alt/view/AddingAppointmentView.fxml");
    }

    /**
     * This method will navigate to the Editing Appointment View. In which the user can edit an existing appointment
     * in the appointment table view. This Editing Appointment View form will have an appointment id that is
     * going to be auto generated, title, description, location and type. The start date time and end date time
     * will be broken down into two parts. The start/end date of the appointment and start/end time of the appointment.
     * Then there will be a checkbox for both existing client and stylist.
     * @param actionEvent the action event will be triggered once the adding appointment button is clicked.
     */
    @FXML
    public void onActionEditingAppointmentButton(ActionEvent actionEvent) {

        if (appointmentModelTableViewTableView.getSelectionModel().isEmpty()) {
            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.setTitle("Error");
            alert.setHeaderText("No Appointment is Selected!");
            alert.setContentText("Please make sure, you have selected a appointment from the row.");
            alert.showAndWait();
        } else {
            AppointmentModel appointmentModelEditing = appointmentModelTableViewTableView.getSelectionModel().getSelectedItem();

            try {
                Stage stage = (Stage) ((Node) actionEvent.getSource()).getScene().getWindow();

                if(appointmentModelEditing == null) {
                    System.out.println("Selected Appointment is missing.");
                    return;
                }

                FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("/christopher/lim/c195alt/view/EditingAppointmentView.fxml"));
                stage.setScene(new Scene(fxmlLoader.load()));

                EditingAppointmentViewController editingAppointmentViewController = fxmlLoader.getController();
                editingAppointmentViewController.editingInitialForm(appointmentModelEditing);
                stage.show();
            } catch (IOException e) {
                System.out.println(e);
            }
        }
    }

    /**
     * This method will delete the selected appointment from the table. If the user did not select
     * any appointment from the table. No appointments will be deleted and it will show an alert message.
     * And if the user selected an appointment from the table. Before making a sql query to delete the appointment
     * an alert will be prompt. If the user choose confirm, the appointment will be deleted. If they choose cancel,
     * the appointment will remain in the table.
     * @param actionEvent the action event will be triggered once the user click the removing appointment button
     * @throws IOException will be triggered if I/O exception to some sort has occurred.
     */
    @FXML
    public void onActionRemovingAppointmentButton(ActionEvent actionEvent) throws IOException {
        if (appointmentModelTableViewTableView.getSelectionModel().isEmpty()) {
            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.setTitle("Error");
            alert.setHeaderText("No Appointment is Selected!");
            alert.setContentText("Please make sure, you have selected a Appointment from the row");
            alert.showAndWait();
        } else {
            AppointmentModel appointmentModel = appointmentModelTableViewTableView.getSelectionModel().getSelectedItem();

            int appointmentId = appointmentModel.getIdappt();
            String appointmentTitle = appointmentModel.getTitle();
            String appointmentType = appointmentModel.getType();

            Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
            alert.setTitle("Appointment Deletion Confirmation");
            alert.setHeaderText("Delete Appointment?");
            alert.setContentText("Are you sure, you want to delete this Appointment? \n" +
                    "Appointment Id: " + appointmentId +
            "\nAppointment Title: " + appointmentTitle +
                    "\nAppointment Type: " + appointmentType);

            Optional<ButtonType> buttonAction = alert.showAndWait();

            if (buttonAction.isPresent() && buttonAction.get() == ButtonType.OK) {
                try {
                    if (AppointmentSQLQuery.deletingAppointmentFromAltDB(appointmentId)) {
                        System.out.println("Appointment Id: " + appointmentId + ", Appointment Title: " + appointmentTitle + " is now deleted!");
                    } else {
                        System.out.println("Appointment Id: " + appointmentId +", Appointment Title: " + appointmentTitle + "\nAppointment Type: " + appointmentType + " is NOT deleted!");
                    }
                    appointmentModelTableViewTableView.setItems(AppointmentSQLQuery.getAllAppointmentQuery());
                } catch (SQLException e) {
                    throw new RuntimeException(e);
                }
            }
        }
    }

    /**
     * This method is used to sort the appointment table by all records, by the current month
     * and by the current week. By default, it will select all records.
     * @param actionEvent the action event will be triggered when the user selected the sort
     * from the selection.
     * @throws SQLException this will give out information about database access error
     */
    public void  onActionSortBy(ActionEvent actionEvent) throws SQLException {
        ObservableList<AppointmentModel> observableList;

        if (appointmentViewSortBy.getValue().equals("All")) {
            Alert alert = new Alert(Alert.AlertType.INFORMATION);
            alert.setTitle("Appointment");
            alert.setHeaderText("All Appointment Record");
            alert.setContentText("You are viewing all records of the appointment.");
            alert.showAndWait();

            observableList = AppointmentSQLQuery.getAllAppointmentQuery();
            appointmentModelTableViewTableView.setItems(observableList);
        } else if (appointmentViewSortBy.getValue().equals("Month")) {
            YearMonth currentMonth = YearMonth.now();
            LocalDate startDate = currentMonth.atDay(1);
            LocalDate endDate = currentMonth.atEndOfMonth();

            Alert alert = new Alert(Alert.AlertType.INFORMATION);
            alert.setTitle("Appointment");
            alert.setHeaderText("Current Month Appointment Record");
            alert.setContentText("You are viewing the current month records of the appointment.\n"
            + "Current Month: " + currentMonth + "\n" + "First Day of the Month: " + startDate + "\nLast Day of the Month: " + endDate );
            alert.showAndWait();

            System.out.println("Current Month: " + currentMonth + "\n" + "First Day of the Month: " + startDate + "\nLast Day of the Month: " + endDate);
            observableList = AppointmentSQLQuery.sortingAppointmentBy(startDate, endDate);
            appointmentModelTableViewTableView.setItems(observableList);
        } else if (appointmentViewSortBy.getValue().equals("Week")) {
            LocalDate currentDay = LocalDate.now();
            LocalDate startDay = currentDay.with(TemporalAdjusters.previousOrSame(DayOfWeek.SUNDAY));
            LocalDate endDay = currentDay.with(TemporalAdjusters.nextOrSame(DayOfWeek.SATURDAY));

            Alert alert = new Alert(Alert.AlertType.INFORMATION);
            alert.setTitle("Appointment");
            alert.setHeaderText("Current Week Appointment Record");
            alert.setContentText("You are viewing the current week records of the appointment.\n"
                    + "Current Day: " + currentDay + "\n" + "Start Day of the Week: " + startDay + "\nEnd Day of the Week: " + endDay);
            alert.showAndWait();

            System.out.println("Current Day: " + currentDay + "\n" + "Start Day of the Week: " + startDay + "\nEnd Day of the Week: " + endDay);
            observableList = AppointmentSQLQuery.sortingAppointmentBy(startDay, endDay);
            appointmentModelTableViewTableView.setItems(observableList);
        }
    }

    /**
     * This method will log out the User and navigate them back to the Login View
     * @param actionEvent the action event is being triggered when the Log out button is clicked.
     * @throws IOException will be triggered if I/O exception to some sort has occurred.
     */
    @FXML
    public void onActionLogoutButton(ActionEvent actionEvent) throws IOException {
        Utilities.navigateApplicationView(actionEvent, "/christopher/lim/c195alt/view/LoginView.fxml");
    }

    /**
     * This method will close the whole application.
     * @param actionEvent the action event is being triggered when the exit button is clicked.
     * @throws IOException will be triggered if I/O exception to some sort has occurred.
     */
    @FXML
    public void onActionExitButton(ActionEvent actionEvent) throws IOException {
        System.exit(0);
    }


    /**
     * This initializes the Appointment Table View page
     * @param url this sets the url
     * @param resourceBundle this sets the resource bundle
     */
    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        try {
            ObservableList<AppointmentModel> appointmentModels = AppointmentSQLQuery.getAllAppointmentQuery();
            appointmentModelTableViewTableView.setItems(appointmentModels);
            appointmentViewSortBy.setItems(FXCollections.observableArrayList(sortName));
            appointmentViewSortBy.getSelectionModel().selectFirst();
        } catch (SQLException e) {
            System.out.println(e);
            throw new RuntimeException(e);
        }


        // This sets the cell value of the table for Appointments
        appointmentViewAppointmentIdColumn.setCellValueFactory(new PropertyValueFactory<>("idappt"));
        appointmentViewTitleColumn.setCellValueFactory(new PropertyValueFactory<>("title"));
        appointmentViewDescriptionColumn.setCellValueFactory(new PropertyValueFactory<>("descr"));
        appointmentViewLocationColumn.setCellValueFactory(new PropertyValueFactory<>("location"));
        appointmentViewTypeColumn.setCellValueFactory(new PropertyValueFactory<>("type"));
        appointmentViewStartColumn.setCellValueFactory(new PropertyValueFactory<>("start"));
        appointmentViewEndColumn.setCellValueFactory(new PropertyValueFactory<>("end"));
        appointmentViewClientIdColumn.setCellValueFactory(new PropertyValueFactory<>("clientid"));
        appointmentViewStylistIdColumn.setCellValueFactory(new PropertyValueFactory<>("stylid"));
    }
}
