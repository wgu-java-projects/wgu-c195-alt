package christopher.lim.c195alt.appointment;

import christopher.lim.c195alt.client.ClientSQLQuery;
import christopher.lim.c195alt.helper.Utilities;
import christopher.lim.c195alt.client.ClientModel;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.*;

import java.io.IOException;
import java.net.URL;
import java.sql.SQLException;
import java.time.*;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoField;
import java.util.ResourceBundle;
import java.util.stream.Stream;

/**
 * This controller class is for the Editing Appointment View that will be used
 * to edit existing Appointment from the altdb. And it will edit the existing record
 * to the Appointment Table View.
 * @author Christopher Lim
 */
public class EditingAppointmentViewController implements Initializable {
    // These are the list of fx:id that will be used for the editing client view
    @FXML private TextField editingAppointmentViewApptId;
    @FXML private TextField editingAppointmentViewTitle;
    @FXML private TextField editingAppointmentViewDescription;
    @FXML private TextField editingAppointmentViewLocation;
    @FXML private TextField editingAppointmentViewType;
    @FXML private DatePicker editingAppointmentViewStartDate;
    @FXML private DatePicker editingAppointmentViewEndDate;
    @FXML private ComboBox<String> editingAppointmentViewStartTime;
    @FXML private ComboBox<String> editingAppointmentViewEndTime;
    @FXML private ComboBox<ClientModel> editingAppointmentViewClient;
    @FXML private ComboBox<StylistModel> editingAppointmentViewStylist;

    /**
     * This method will do a validation first to see if all fields are not empty. And then it will check if
     * the appointment start date is not after the appointment end date. After that, it will check if the
     * appointment date falls in the observe business holiday. Next, it will check if the appointment falls
     * on Weekdays or not. After that it will start checking the start time and end time for validation. Such as
     * if the start time is after the end time. And if both are in the Business Hours range of 8:00 - 22:00 MTS.
     * Lastly, it will check if the appointment we are creating have some overlaps. If everything is passes, there
     * will be an alert modal will notify the user to proceed or not. If clicking Cancel, then nothing
     * will happen. And if the user clicks Ok, then it will update the selected appointment and navigate back to the Appointments
     * table.
     * @param actionEvent The action event will be triggered when we click the Editing Appointment Button.
     * @throws SQLException this will give out information about database access error
     */
    @FXML
    public void onActionEditingAppointmentButton(ActionEvent actionEvent) throws SQLException {
        // The fields for grabbing the data from the text field
        int appointmentId = Integer.parseInt(editingAppointmentViewApptId.getText());
        String appointmentTitle = editingAppointmentViewTitle.getText();
        String appointmentDescription = editingAppointmentViewDescription.getText();
        String appointmentLocation = editingAppointmentViewLocation.getText();
        String appointmentType = editingAppointmentViewType.getText();

        // The fields for grabbing the date from the date picker
        LocalDate appointmentStartLocalDate = editingAppointmentViewStartDate.getValue();
        LocalDate appointmentEndLocalDate = editingAppointmentViewEndDate.getValue();

        // The fields for grabbing the time from the combo box
        String appointmentStartTime = editingAppointmentViewStartTime.getValue();
        String appointmentEndTime = editingAppointmentViewEndTime.getValue();

        // The fields for grabbing the data from the combo box
        int appointmentViewClient = editingAppointmentViewClient.getSelectionModel().getSelectedItem().getIdclient();
        int appointmentViewStylist = editingAppointmentViewStylist.getSelectionModel().getSelectedItem().getIdstylist();

        // This parses appointmentStartTime and appointmentEndTime into a LocalTime
        LocalTime apptLocalStartTime = LocalTime.parse(appointmentStartTime);
        LocalTime apptLocalEndTime = LocalTime.parse(appointmentEndTime);

        // This parses appointmentStartLocalDate, appointmentEndLocalDate, apptLocalStartTime and apptLocalEndTime
        // into LocalDateTime
        LocalDateTime apptLocalStartDateTime = LocalDateTime.parse(appointmentStartLocalDate + " " +
                apptLocalStartTime + ":00", DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss"));
        LocalDateTime apptLocalEndDateTime = LocalDateTime.parse(appointmentEndLocalDate + " " +
                apptLocalEndTime + ":00", DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss"));

        ObservableList<AppointmentModel> getAllAppointments = AppointmentSQLQuery.getAllAppointmentQuery();

        // This if statement checks for blank and empty text fields
        if ((appointmentTitle.isEmpty() || appointmentTitle.isBlank()) || (appointmentDescription.isEmpty() || appointmentDescription.isBlank())
                || (appointmentLocation.isEmpty() || appointmentLocation.isBlank()) || (appointmentType.isEmpty() || appointmentType.isBlank())) {
            // This alert will display the Error
            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.setTitle("Appointment Information Error");
            alert.setHeaderText("Fields Alert!");
            alert.setContentText("Please make sure the all fields are filled!");
            alert.showAndWait();
            return;
        }


        // This checks if the start date is after the end date
        if (appointmentStartLocalDate.isAfter(appointmentEndLocalDate)) {
            // This alert will display the error
            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.setTitle("Appointment Information Error");
            alert.setHeaderText("Start Date and End Date Issue!");
            alert.setContentText("Please make sure your Start Date Begins before your End Date.");
            alert.showAndWait();
            return;
        }

        // This checks if the start date or end date falls to the observed holiday
        if (checkObservedHolidays(appointmentStartLocalDate, appointmentEndLocalDate)) {
            // This alert will display the error
            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.setTitle("Appointment Information Error");
            alert.setHeaderText("Observed Holiday Alert!");
            alert.setContentText("Please make sure your Start Date and End Date does not fall in these holidays.\n"
                    + "Thanksgiving and the Friday after, July 4th  and January 1st.");
            alert.showAndWait();
            return;
        }


        // This checks if both start date and end date are within the business days
//        if (checkAppointmentWithinBusinessDays(appointmentStartLocalDate, appointmentEndLocalDate)) {
//            // This alert will display the Error
//            Alert alert = new Alert(Alert.AlertType.ERROR);
//            alert.setTitle("Appointment Information Error");
//            alert.setHeaderText("Date Picker Alert!");
//            alert.setContentText("Please make sure that the Business Days are from Monday to Friday only.");
//            alert.showAndWait();
//            return;
//        }

        // This checks if the start time is after the end time
        if (apptLocalStartTime.isAfter(apptLocalEndTime)) {
            // This alert will display the error
            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.setTitle("Appointment Information Error");
            alert.setHeaderText("Start Time Error");
            alert.setContentText("Please make sure your start time is not after the end time.");
            alert.showAndWait();
            return;
        }

        // This checks if the start time is the same as the end time
        if (apptLocalStartTime.equals(apptLocalEndTime)) {
            // This alert will display the error
            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.setTitle("Appointment Information Error");
            alert.setHeaderText("Start Time Error");
            alert.setContentText("Please make sure your start time is not the same time as your end time.");
            alert.showAndWait();
            return;
        }

        // This checks if the start time and end time is within the business hours
        if (!checkAppointmentWithinBusinessTime(apptLocalStartDateTime, apptLocalEndDateTime)) {
            // This alert will display the error
            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.setTitle("Appointment Information Error");
            alert.setHeaderText("Business Hours Alert!");
            alert.setContentText("Please make sure your Start Time and End Time are within the Business Hours of 8:00 AM to 10:00 PM MTS.");
            alert.showAndWait();
            return;
        }

        // This checks for appointments overlaps
        for (AppointmentModel appointmentModel : getAllAppointments) {
            LocalDateTime validateStartDateTime = appointmentModel.getStart();
            LocalDateTime validateEndDateTime = appointmentModel.getEnd();

            if ((appointmentViewClient == appointmentModel.getClientid()) && (appointmentId != appointmentModel.getIdappt()) &&
                    (apptLocalStartDateTime.isBefore(validateStartDateTime)) && (apptLocalEndDateTime.isAfter(validateEndDateTime))) {
                // This alert will display the error
                Alert alert = new Alert(Alert.AlertType.ERROR);
                alert.setTitle("Appointment Information Error");
                alert.setHeaderText("Appointment Overlap Alert!");
                alert.setContentText("Please make sure your appointment is not overlapping by changing the date and time.");
                alert.showAndWait();
                return;
            }

            if ((appointmentViewClient == appointmentModel.getClientid()) && (appointmentId != appointmentModel.getIdappt()) &&
                    (apptLocalStartDateTime.isAfter(validateStartDateTime)) && (apptLocalStartDateTime.isBefore(validateEndDateTime))) {
                // This alert will display the error
                Alert alert = new Alert(Alert.AlertType.ERROR);
                alert.setTitle("Appointment Information Error");
                alert.setHeaderText("Appointment Overlap Alert!");
                alert.setContentText("Please make sure your appointment is not overlapping by changing the date and time.");
                alert.showAndWait();
                return;
            }

            if (appointmentViewClient == appointmentModel.getClientid() && (appointmentId != appointmentModel.getIdappt()) &&
                    (apptLocalEndDateTime.isAfter(validateStartDateTime)) && (apptLocalEndDateTime.isBefore(validateEndDateTime))) {
                // This alert will display the error
                Alert alert = new Alert(Alert.AlertType.ERROR);
                alert.setTitle("Appointment Information Error");
                alert.setHeaderText("Appointment Overlap Alert!");
                alert.setContentText("Please make sure your appointment is not overlapping by changing the date and time.");
                alert.showAndWait();
                return;
            }
        }


        LocalDateTime utcStartDateTime = convertMountainStandardTimeToUTC(apptLocalStartDateTime);
        LocalDateTime utcEndDateTime = convertMountainStandardTimeToUTC(apptLocalEndDateTime);


        // This alert will display the information
        Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
        alert.setTitle("Appointment Information Confirmation");
        alert.setHeaderText("Do you want to Edit this appointment?");
        alert.setContentText("Appointment Id: " + appointmentId +
                "\nTitle: " + appointmentTitle+ "\nDescription: " + appointmentDescription
                + "\nLocation: " + appointmentLocation + "\nType: " + appointmentType
                + "\nStart Date and Time: " + apptLocalStartDateTime + "\nEnd Date and Time: " + apptLocalEndDateTime
                +"\nClient: " + appointmentViewClient + "\nStylist: " + appointmentViewStylist);

        alert.showAndWait().ifPresent((confirmButton -> {
            if (confirmButton == ButtonType.OK) {
                try {
                    System.out.println("UTC Start Time: " + utcStartDateTime + ", Updating to the Database.");
                    System.out.println("UTC End Time: " + utcEndDateTime + ", Updating to the Database.");
                    AppointmentSQLQuery.updatingAppointmentFromAltDB(new AppointmentModel(appointmentId, appointmentTitle, appointmentDescription, appointmentLocation,
                            appointmentType, apptLocalStartDateTime, apptLocalEndDateTime, appointmentViewClient, appointmentViewStylist));
                    Utilities.navigateApplicationView(actionEvent, "/christopher/lim/c195alt/view/AppointmentTableView.fxml");
                } catch (SQLException | IOException e) {
                    throw new RuntimeException(e);
                }
            }
        }));
    }

    /**
     * This method will check if convert the local date and time into Mountain Standard Time.
     * And checks the time if the start time is not before 8:00 AM and the end time not after 10:00 PM MST
     * @param localDateTime the local date time parameter is passed by the new appointment model
     * @return if the boolean returns true then we can create the appointment and if it returns false
     * we do not create the appointment
     */
    public static boolean salonBusinessHours(LocalDateTime localDateTime) {
        ZonedDateTime mountainStandardTime = localDateTime.atZone(ZoneId.systemDefault()).withZoneSameInstant(ZoneId.of("America/Phoenix"));
        LocalTime localTime = mountainStandardTime.toLocalTime();
        return !localTime.isBefore(LocalTime.of(8, 0) ) && !localTime.isAfter(LocalTime.of(22, 0));
    }

    /**
     * This method will convert the Mountain Standard Time to UTC. So that, we can
     * insert the localDateTime time as UTC to the Database.
     * @param localDateTime this param will be either start date time or end date time
     * @return this will return the converted localDateTime
     */
    public static LocalDateTime convertMountainStandardTimeToUTC(LocalDateTime localDateTime) {
        ZonedDateTime utc = localDateTime.atZone(ZoneId.systemDefault()).withZoneSameInstant(ZoneId.of("UTC"));
        return utc.toLocalDateTime();
    }

    /**
     * This method will check if the Appointment Start Date and End Date is within the
     * Business days of the salon. If the selected Date is not in the Business days,
     * it will show the alert message.
     * @param startDate this will pass the param for start date of the appointment
     * @param endDate this will pass the param for end date of the appointment
     * @return this will return true or false. If it's true do not create the appointment
     * and if it's false then create the appointment
     */
    public static boolean checkAppointmentWithinBusinessDays(LocalDate startDate, LocalDate endDate) {
        int workWeekStartDay = DayOfWeek.MONDAY.getValue();
        int workWeekEndDay = DayOfWeek.FRIDAY.getValue();

        DayOfWeek appointmentStartDay = startDate.getDayOfWeek();
        DayOfWeek appointmentEndDay = endDate.getDayOfWeek();

        System.out.println("Start Day: " + appointmentStartDay);
        System.out.println("End Day: " + appointmentEndDay);

        return appointmentStartDay.getValue() < workWeekStartDay || appointmentStartDay.getValue() > workWeekEndDay || appointmentEndDay.getValue() < workWeekStartDay || appointmentEndDay.getValue() > workWeekEndDay;
    }

    /**
     * This method will check if convert the local date and time into Mountain Standard Time.
     * And checks the time if the start time is not before 8:00 AM and the end time not after 10:00 PM MST
     * @param startDateTime this will pass the start time of the appointment
     * @param endDateTime this will pass the end time of the appointment
     * @return it will return true or false. If its true, then both start time and end time are on the business hours.
     * And if it returns false, then both start and end time are not in the same range.
     */
    public static boolean checkAppointmentWithinBusinessTime(LocalDateTime startDateTime, LocalDateTime endDateTime) {
        return salonBusinessHours(startDateTime) && salonBusinessHours(endDateTime);
    }

    /**
     * This method will check if the start date and end date will fall on the observed holidays by the salon.
     * If it does it will return true and the user cannot create an appointment.
     * @param startDate this param will handle the start date for the appointment
     * @param endDate this param will handle the end date for the appointment
     * @return it will return true or false. If true, do not create the appointment.
     * And if it false, let the User create the appointment.
     */
    public static boolean checkObservedHolidays(LocalDate startDate, LocalDate endDate) {
        // Start Date
        Month currentStartDateMonth = startDate.getMonth();
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("MM/dd");
        DayOfWeek currentStartDateDayOfWeek = startDate.getDayOfWeek();

        LocalDate tomorrowStartDate = startDate;
        tomorrowStartDate = tomorrowStartDate.plusDays(1);

        LocalDate yesterdayStartDate = startDate;
        yesterdayStartDate = yesterdayStartDate.minusDays(1);
        DayOfWeek previousStartDayOfWeek = yesterdayStartDate.getDayOfWeek();

        // End Date
        Month currentEndDateMonth = endDate.getMonth();
        DayOfWeek currentEndDateDayOfWeek = endDate.getDayOfWeek();

        LocalDate tomorrowEndDate = endDate;
        tomorrowEndDate = tomorrowEndDate.plusDays(1);

        LocalDate yesterdayEndDate = endDate;
        yesterdayEndDate = yesterdayEndDate.minusDays(1);
        DayOfWeek previousEndDayOfWeek = yesterdayEndDate.getDayOfWeek();

        // Validate if both start date and end date are New Year
        if(formatter.format(startDate).equals("01/01") || formatter.format(endDate).equals("01/01")){
            return true;
            // Validate if both start date and end date are Monday
        } else if (formatter.format(startDate).equals("01/02") && currentStartDateDayOfWeek.equals(DayOfWeek.MONDAY)
                || formatter.format(endDate).equals("01/02") && currentEndDateDayOfWeek.equals(DayOfWeek.MONDAY)) {
            return true;
            // Validate if both start date and end date are Fourth of July
        } else if (formatter.format(startDate).equals("07/04") || formatter.format(endDate).equals("07/04")){
            return true;
            // Validate if both start date and end date are July 3rd and falls Friday
        } else if (formatter.format(startDate).equals("07/03") && currentStartDateDayOfWeek.equals(DayOfWeek.FRIDAY)
                || formatter.format(endDate).equals("07/03") && currentEndDateDayOfWeek.equals(DayOfWeek.FRIDAY)) {
            return true;
            // Validate if both start date and end date are going to fall for the 4th Week Thursday of the Month
            // November for Thanksgiving
        } else if(currentStartDateMonth.equals(Month.NOVEMBER) && startDate.get(ChronoField.ALIGNED_WEEK_OF_MONTH) == 4
                && currentStartDateDayOfWeek.equals(DayOfWeek.THURSDAY) ||
                currentEndDateMonth.equals(Month.NOVEMBER) && endDate.get(ChronoField.ALIGNED_WEEK_OF_MONTH) == 4
                        && currentEndDateDayOfWeek.equals(DayOfWeek.THURSDAY)){
            return true;
            // Validate if both start date and end date are going to fall Friday after Thanksgiving
        } else if ((currentStartDateMonth.equals(Month.NOVEMBER) && yesterdayStartDate.get(ChronoField.ALIGNED_WEEK_OF_MONTH) == 4
                && previousStartDayOfWeek.equals(DayOfWeek.THURSDAY)) && currentStartDateDayOfWeek.equals(DayOfWeek.FRIDAY) ||
                (currentEndDateMonth.equals(Month.NOVEMBER) && yesterdayEndDate.get(ChronoField.ALIGNED_WEEK_OF_MONTH) == 4
                        && previousEndDayOfWeek.equals(DayOfWeek.THURSDAY)) && currentEndDateDayOfWeek.equals(DayOfWeek.FRIDAY)) {
            return true;
        }
        return false;
    }


    /**
     * This method will cancel editing the appointment form and will navigate back to the Appointment Table View
     * @param actionEvent the event will be triggered when the Cancel Editing Appointment is clicked
     * @throws IOException will be triggered if I/O exception to some sort has occurred.
     */
    @FXML
    public void onActionCancelEditingAppointmentButton(ActionEvent actionEvent) throws IOException {
        Utilities.navigateApplicationView(actionEvent, "/christopher/lim/c195alt/view/AppointmentTableView.fxml");
    }

    /**
     * This method will initiate the fields, calendar date and dropdown for the
     * Appointment object.
     * @param appointmentModelEditing the appointmentModelEditing will access the Appointment
     * model to fill up the fields, calendar date and dropdown.
     */
    public void editingInitialForm(AppointmentModel appointmentModelEditing) {
        // This will fill up the Appointment Update form
        editingAppointmentViewApptId.setText(Integer.toString(appointmentModelEditing.getIdappt()));
        editingAppointmentViewTitle.setText(appointmentModelEditing.getTitle());
        editingAppointmentViewDescription.setText(appointmentModelEditing.getDescr());
        editingAppointmentViewLocation.setText(appointmentModelEditing.getLocation());
        editingAppointmentViewType.setText(appointmentModelEditing.getType());

        // Since we combine the Date and Time to insert the start date time and end date time.
        // We need to separate the date and time
        System.out.println("Local Start Time: " + localDateTimeStringConverter(appointmentModelEditing.getStart(), 1));
        System.out.println("Local End Time: " + localDateTimeStringConverter(appointmentModelEditing.getEnd(), 1));
        editingAppointmentViewStartDate.setValue(LocalDate.parse(localDateTimeStringConverter(appointmentModelEditing.getStart(), 0)));
        editingAppointmentViewEndDate.setValue(LocalDate.parse(localDateTimeStringConverter(appointmentModelEditing.getEnd(), 0)));
        editingAppointmentViewStartTime.setValue(localDateTimeStringConverter(appointmentModelEditing.getStart(), 1));
        editingAppointmentViewEndTime.setValue(localDateTimeStringConverter(appointmentModelEditing.getEnd(), 1));

        // This will select the id's from the selected appointment
        editingAppointmentViewClient.getSelectionModel().select(appointmentModelEditing.getClientid() - 1);
        editingAppointmentViewStylist.getSelectionModel().select(appointmentModelEditing.getStylid() - 1);
    }

    /**
     * This method will convert the local date time and split the scheduled date and scheduled time.
     * And then if we pass an int of 0 it will return the date and if we enter int of 1 it will be
     * returning the time
     * @param localDateTime the localDateTime will come from the Start and End LocalDateTime
     * @param number the number will return 1 or 0
     * @return this will return the arrSelection that will have an array value of 0 or 1
     */
    public String localDateTimeStringConverter (LocalDateTime localDateTime, int number) {
        DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm");
        String formattedDateTime = localDateTime.format(dateTimeFormatter);

        String[] arrSelection = formattedDateTime.split(" ");

        return arrSelection[number];
    }

    /**
     * This initializes the Editing Appointment Table View page
     * @param url this sets the url
     * @param resourceBundle this sets the resource bundle
     */
    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        try {
            editingAppointmentViewClient.setItems(ClientSQLQuery.getAllClientQuery());
            editingAppointmentViewClient.getSelectionModel().selectFirst();
            editingAppointmentViewStylist.setItems(StylistSQLQuery.getAllStylistQuery());

            ObservableList<String> businessHours = FXCollections.observableArrayList();
            LocalTime businessHoursStart = LocalTime.MIN.plusHours(8);
            LocalTime businessHoursEnd = LocalTime.MAX.minusHours(1).minusMinutes(45);

            if (!businessHoursStart.equals(0) || !businessHoursEnd.equals(0)) {
                while (businessHoursStart.isBefore(businessHoursEnd)) {
                    businessHours.add(String.valueOf(businessHoursStart));
                    businessHoursStart = businessHoursStart.plusMinutes(15);
                }
            }

            editingAppointmentViewStartTime.setItems(businessHours);
            editingAppointmentViewStartTime.getSelectionModel().selectFirst();
            editingAppointmentViewEndTime.setItems(businessHours);
            editingAppointmentViewEndTime.getSelectionModel().select(4);
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }
}
