package christopher.lim.c195alt.appointment;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import static christopher.lim.c195alt.helper.JavaDataBaseConnectivity.connection;

/**
 * This class will handle the Stylist query from the altdb.
 * @author Christopher Lim
 */
public class StylistSQLQuery {

    /**
     * This method will run the query in the altdb to get all the Stylist.
     * After that, it will add the result to the selectAllStylistObservationList.
     * In which will return the list of Stylist and will be displayed on
     * adding and editing appointment combo box.
     * @return the selectAllStylistObservationList will be returned.
     * @throws SQLException this will give out information about database access error
     */
    public static ObservableList<StylistModel> getAllStylistQuery() throws SQLException {
        // These are fields that will be used in query
        ObservableList<StylistModel> selectAllStylistObservationList = FXCollections.observableArrayList();
        String sql = "SELECT * FROM altdb.stylist";

        PreparedStatement sqlStatement = connection.prepareStatement(sql);
        ResultSet resultSet = sqlStatement.executeQuery();
        while (resultSet.next()) {
            selectAllStylistObservationList.add(buildStylistModelFromResultSet(resultSet));
        }

        return selectAllStylistObservationList;
    }

    /**
     * This method will build the Stylist Model by grabbing the stylist id and name from
     * the altdb Stylist table.
     * @param resultSet will set the result set of the sql query
     * @return This will return the new Stylist Model with the id and name
     * @throws SQLException this will give out information about database access error
     */
    public static StylistModel buildStylistModelFromResultSet(ResultSet resultSet) throws SQLException {
        // These are fields to get the stylist id and name
        int idstylist = resultSet.getInt("idstylist");
        String name = resultSet.getString("name");

        System.out.println("Stylist Id: " + idstylist + ", Name: " + name);
        return new StylistModel(idstylist, name);
    }
}
