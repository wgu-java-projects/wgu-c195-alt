package christopher.lim.c195alt.appointment;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

import static christopher.lim.c195alt.helper.JavaDataBaseConnectivity.connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

/**
 * This class will handle the Appointment query from the altdb.
 * @author Christopher Lim
 */
public class AppointmentSQLQuery {

    /**
     * This method will query the Appointment table with the current
     * Max Appointment id. Then after finding the number, we grab the value
     * and add one to increment the number.
     * @return It will return idappt + 1 if the query found the max value.
     * @throws SQLException this will give out information about database access error
     */
    public static int getAppointmentMaxId() throws SQLException {
        // These are fields that will be used in query
        String sql = "SELECT MAX(idappt) FROM altdb.appt";

        try (PreparedStatement sqlStatement = connection.prepareStatement(sql);) {
            ResultSet resultSet = sqlStatement.executeQuery();
            if (resultSet.next()) {
                System.out.println("Max appointment id: " + resultSet.getInt("MAX(idappt)"));
                return resultSet.getInt("MAX(idappt)") + 1;
            }
        } catch (SQLException e) {
            System.out.println(e);
        }

        return 1;
    }

    /**
     * This method will run a sql query to insert the new appointment model to the appointment table.
     * If successful, we will be able to see the new appointment show up in the appointment table.
     * @param appointmentModel this param is where we past the new appointment model
     * @return the addNewAppointmentRow will be added to the appointment table
     * @throws SQLException this will give out information about database access error
     */
    public static boolean addingAppointmentToAltDB(AppointmentModel appointmentModel) throws SQLException {
        // These are fields that will be used in query
        int addNewAppointmentRow = 0;
        String sql = "INSERT INTO altdb.appt (idappt, title, descr, location, type, start, end, clientid, stylid) VALUES (?,?,?,?,?,?,?,?,?)";

        try (PreparedStatement sqlStatement = connection.prepareStatement(sql)) {
            sqlStatement.setInt(1, appointmentModel.getIdappt());
            sqlStatement.setString(2, appointmentModel.getTitle());
            sqlStatement.setString(3, appointmentModel.getDescr());
            sqlStatement.setString(4, appointmentModel.getLocation());
            sqlStatement.setString(5, appointmentModel.getType());
            sqlStatement.setTimestamp(6, Timestamp.valueOf(appointmentModel.getStart()));
            sqlStatement.setTimestamp(7, Timestamp.valueOf(appointmentModel.getEnd()));
            sqlStatement.setInt(8, appointmentModel.getClientid());
            sqlStatement.setInt(9, appointmentModel.getStylid());


            addNewAppointmentRow = sqlStatement.executeUpdate();

            System.out.println(addNewAppointmentRow);
        } catch (SQLException e) {
            System.out.println("Method addingAppointmentToAltDB: " + e);
            throw e;
        }

        return addNewAppointmentRow > 0;
    }

    /**
     * This method will run a sql query to update the appointment from the altdb.
     * And if successful, the selected appointment will be updated.
     * @param appointmentModel this param is where we pass update the appointment model
     * @return the updatingAppointmentRow will be updating the selected table
     * @throws SQLException this will give out information about database access error
     */
    public static boolean updatingAppointmentFromAltDB(AppointmentModel appointmentModel) throws SQLException {
        // These are fields that will be used in query
        int updatingAppointmentRow = 0;
        String sql = "UPDATE altdb.appt SET title = ?, descr = ?, location = ?, type = ?, start = ?, end = ?, clientid = ?, stylid= ? WHERE idappt = ?";

        try (PreparedStatement sqlStatement = connection.prepareStatement(sql)) {
            sqlStatement.setString(1, appointmentModel.getTitle());
            sqlStatement.setString(2, appointmentModel.getDescr());
            sqlStatement.setString(3, appointmentModel.getLocation());
            sqlStatement.setString(4, appointmentModel.getType());
            sqlStatement.setTimestamp(5, Timestamp.valueOf(appointmentModel.getStart()));
            sqlStatement.setTimestamp(6, Timestamp.valueOf(appointmentModel.getEnd()));
            sqlStatement.setInt(7, appointmentModel.getClientid());
            sqlStatement.setInt(8, appointmentModel.getStylid());
            sqlStatement.setInt(9, appointmentModel.getIdappt());

            updatingAppointmentRow = sqlStatement.executeUpdate();

            System.out.println(updatingAppointmentRow);
        } catch (SQLException e) {
            System.out.println("Method updatingClientFromAltDB: " + e);
            throw e;
        }

        return updatingAppointmentRow > 0;
    }


    /**
     * This method will delete the appointment from the appointment table
     * after it make a DELETE sql with an idappt.
     * @param deleteAppointmentId this param will be used to pass the appointment id.
     * @return This will return deleteAppointmentRow by deleting the selected appointment id
     * @throws SQLException this will give out information about database access error
     */
    public static boolean deletingAppointmentFromAltDB(int deleteAppointmentId) throws SQLException {
        // These are fields that will be used in query
        int deleteAppointmentRow = 0;
        String sql = "DELETE FROM altdb.appt WHERE idappt = ?";

        try (PreparedStatement sqlStatement = connection.prepareStatement(sql)){
            sqlStatement.setInt(1, deleteAppointmentId);
            deleteAppointmentRow = sqlStatement.executeUpdate();
            System.out.println(deleteAppointmentRow);
        } catch (SQLException e){
            System.out.println("Method deletingAppointmentFromAltDB: " + e);
            throw e;
        }

        return deleteAppointmentRow > 0;
    }

    /**
     * This method will run the query to get all appointment from the altdb, and then
     * it will display the data in the Appointment Table View.
     * @return this will return the selectAllAppointmentObservationList that store the sql
     * object we just query
     * @throws SQLException this will give out information about database access error
     */
    public static ObservableList<AppointmentModel> getAllAppointmentQuery() throws SQLException {
        // These are fields that will be used in query
        ObservableList<AppointmentModel> selectAllAppointmentObservationList = FXCollections.observableArrayList();
        String sql = "SELECT * FROM altdb.appt";

        PreparedStatement sqlStatement = connection.prepareStatement(sql);
        ResultSet resultSet = sqlStatement.executeQuery();
        while (resultSet.next()) {
            selectAllAppointmentObservationList.add(buildAppointmentModelFromResultSet(resultSet));
        }

        return selectAllAppointmentObservationList;
    }

    /**
     * This method will make a query search for the appointment start date and end date. By passing
     * which the start parameter and end parameter. After the query search, it will show
     * the appointments that you query for those specific dates.
     * @param start the local date we want to start the query
     * @param end the local date we want to end the query
     * @return this will return the selectAllAppointmentObservationList that will
     * be pass to the Appointment Model to generate the new list.
     */
    public static ObservableList<AppointmentModel> sortingAppointmentBy(LocalDate start, LocalDate end) {
        // These are fields that will be used in query
        ObservableList<AppointmentModel> selectAllAppointmentObservationList = FXCollections.observableArrayList();
        String sql = "SELECT * FROM altdb.appt WHERE DATE(start) BETWEEN ? AND ? ORDER BY idappt DESC";

        try (PreparedStatement sqlStatement = connection.prepareStatement(sql)) {
            sqlStatement.setTimestamp(1, Timestamp.valueOf(start.atStartOfDay()));
            sqlStatement.setTimestamp(2, Timestamp.valueOf(end.atStartOfDay()));
            ResultSet resultSet = sqlStatement.executeQuery();

            while (resultSet.next()) {
                selectAllAppointmentObservationList.add(buildAppointmentModelFromResultSet(resultSet));
            }

        } catch (SQLException e) {
            System.out.println("Method sortingAppointmentBy: " + e);
        }

        return selectAllAppointmentObservationList;
    }

    /**
     * This method will do a query search for the start column with the currentDateTime and appointmentDateTime.
     * If it found anything, it will return the boolean value of not null. And if it does not find
     * any result. It will return the value of null.
     * @return it will return not null if it find any appointments and not it will return null
     * @throws SQLException this will give out information about database access error
     */
    public static AppointmentModel checkAppointmentQuery() throws SQLException {
        // These are fields that will be used in query
        String sql = "SELECT * FROM altdb.appt WHERE start BETWEEN ? AND ? ORDER BY idappt DESC";

        // We will use formatter to make sure that we are passing dates that the SQL database likes
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
        LocalDateTime currentDateTime = LocalDateTime.now();
        LocalDateTime appointmentDateTime = currentDateTime.plusMinutes(15);

        // This is for console logging to check for the output
        System.out.println("\nCurrent Time: " + formatter.format(currentDateTime) +
                "\nCurrent Time with Additional Time: " + formatter.format(appointmentDateTime));

        // This will start doing the sql query
        try (PreparedStatement sqlStatement = connection.prepareStatement(sql)) {
            sqlStatement.setTimestamp(1, Timestamp.valueOf(formatter.format(currentDateTime)));
            sqlStatement.setTimestamp(2, Timestamp.valueOf(formatter.format(appointmentDateTime)));
            ResultSet resultSet = sqlStatement.executeQuery();

            // This will try and generate the result of the query and store it to these fields below
            if (resultSet.next()) {
                int idappt = resultSet.getInt("idappt");
                String title = resultSet.getString("title");
                String descr = resultSet.getString("descr");
                String location = resultSet.getString("location");
                String type = resultSet.getString("type");
                LocalDateTime start = LocalDateTime.parse(resultSet.getString("start"), DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss"));
                LocalDateTime end = LocalDateTime.parse(resultSet.getString("end"), DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss"));
                int clientid = resultSet.getInt("clientid");
                int stylid = resultSet.getInt("stylid");

                // Another logger
                System.out.println("Appt Id: " + idappt + ", title: " + title + ", Description: " + descr + ", Location: "
                        + location + ", Type: " + type + ", Start Date: " + start + ". End Date: " + end + ", Client Id: " + clientid + ", Style Id: " + stylid);

                // This will return the new appointment model that we just made a query
                return new AppointmentModel(idappt, title, descr, location, type, start, end, clientid, stylid);
            }

        } catch (SQLException e) {
            // If there is some SQL exception this will log it and tell use which sql function failed
            System.out.println("Method checkAppointmentQuery: " + e);
        }
        // This will return null if there is no appointment found
        return  null;
    }
    /**
     * This method will build the Appointment by grabbing the appointment id, title, description,
     * location, type, start date time, end date time, client id and style id.
     * @param resultSet will set the result set of the sql query
     * @return This will return the new Appointment Model with the appt. id, title, description,
     * location, type, start date time, end date time, client id and style id.
     * @throws SQLException this will give out information about database access error
     */
    public static AppointmentModel buildAppointmentModelFromResultSet(ResultSet resultSet) throws SQLException {
        // These are fields that will be used to get the appt. id, title, description
        // location, type, start, end, client id, and style id

        int idappt = resultSet.getInt("idappt");
        String title = resultSet.getString("title");
        String descr = resultSet.getString("descr");
        String location = resultSet.getString("location");
        String type = resultSet.getString("type");
        LocalDateTime start = resultSet.getTimestamp("start").toLocalDateTime();
        LocalDateTime end = resultSet.getTimestamp("end").toLocalDateTime();

        int clientid = resultSet.getInt("clientid");
        int stylid = resultSet.getInt("stylid");

        System.out.println("Appt Id: " + idappt + ", title: " + title + ", Description: " + descr + ", Location: "
                + location + ", Type: " + type + ", Start Date: " + start + ". End Date: " + end + ", Client Id: " + clientid + ", Style Id: " + stylid);
        return new AppointmentModel(idappt, title, descr, location, type, start, end, clientid, stylid);
    }

}
