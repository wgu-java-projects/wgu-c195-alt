package christopher.lim.c195alt.appointment;

/**
 * A class to create the Stylist object in the altdb
 * @author Christopher Lim
 */
public class StylistModel {
    private int idstylist;
    private String name;

    /**
     * This is the class constructor for the Stylist Model
     * @param idstylist the stylist id is from the Stylist Model
     * @param name the name of the Stylist
     */
    public StylistModel(int idstylist, String name) {
        this.idstylist = idstylist;
        this.name = name;
    }

    /**
     * This method gets the Stylist Id from the altdb
     * @return the id stylist is being returned as a unique integer
     */
    public int getIdstylist() {
        return idstylist;
    }

    /**
     * This method sets the id of the stylist form
     * @param idstylist the stylist id is sets from the altdb
     */
    public void setIdstylist(int idstylist) {
        this.idstylist = idstylist;
    }

    /**
     * This method gets ths stylist name from the altdb
     * @return the name of the stylist as String
     */
    public String getName() {
        return name;
    }

    /**
     * This method sets the name of the Stylist
     * @param name the name of the Stylist is sets from the altdb
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * This method provides default syntax that will be used from the altdb
     * @return this returns the idstlist and name
     */
    @Override
    public String toString() {
        return name;
    }
}
