module christopher.lim.c195alt {
    requires javafx.controls;
    requires javafx.fxml;
    requires java.sql;
    requires mysql.connector.j;



    opens christopher.lim.c195alt to javafx.fxml;
    exports christopher.lim.c195alt;
    exports christopher.lim.c195alt.helper;
    exports christopher.lim.c195alt.login;
    opens christopher.lim.c195alt.login to javafx.fxml;
    exports christopher.lim.c195alt.client;
    opens christopher.lim.c195alt.client to javafx.fxml;
    exports christopher.lim.c195alt.appointment;
    opens christopher.lim.c195alt.appointment to javafx.fxml;
    exports christopher.lim.c195alt.report;
    opens christopher.lim.c195alt.report to javafx.fxml;
}